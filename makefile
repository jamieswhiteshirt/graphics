CC=clang++

CFLAGS=-c -g -Wall -std=c++11 -D _DEBUG -I$(UTILITY_INCLUDE) $(shell freetype-config --cflags)
SOURCEDIR=src/
OUTDIR=build/
SOURCES=$(wildcard $(SOURCEDIR)*.cpp)
OBJECTS=$(SOURCES:$(SOURCEDIR)%.cpp=$(OUTDIR)%.o)
OUTPUT=$(OUTDIR)libgraphics.a
LIBOUT=/usr/local/lib/libgraphics.a
INCLUDEOUT=/usr/local/include/

all: $(OUTPUT) tools

.PHONY: all

clear:
	rm -rf $(OUTDIR)

.PHONY: clear

rebuild: clear $(OUTPUT)

.PHONY: rebuild

install:
	cp $(OUTPUT) $(LIBOUT)
	cp $(SOURCEDIR)*.h $(INCLUDEOUT)

.PHONY: install

$(OUTPUT): $(OBJECTS)
	ar rcs $(OUTPUT) $(OBJECTS)

$(OUTDIR)%.o:$(SOURCEDIR)%.cpp | $(OUTDIR)
	$(CC) $(CFLAGS) $< -o $@

$(OUTDIR):
	mkdir $(OUTDIR)

tools: objtowfl

.PHONY: tools

objtowfl: $(OUTDIR)objtowfl

.PHONY: objtowfl

$(OUTDIR)objtowfl: $(OUTPUT)
	make -C tools/objtowfl/

.PHONY: $(OUTDIR)objtowfl

