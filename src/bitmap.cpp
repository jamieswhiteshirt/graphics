#include "bitmap.h"
#include <string.h>
#include <algorithm>

namespace Graphics
{
	Bitmap::Bitmap(int width, int height, unsigned char* data)
		: _data(data), _width(width), _height(height)
	{}
	Bitmap::Bitmap(const Bitmap& bitmap)
		: _data(new unsigned char[bitmap._width * bitmap._height * 4]), _width(bitmap._width), _height(bitmap._height)
	{
		memcpy((void*)_data, bitmap._data, bitmap._width * bitmap._height * 4);
	}
	Bitmap::~Bitmap()
	{
		delete[] _data;
	}

	void Bitmap::get(int u, int v, unsigned char& r, unsigned char& g, unsigned char& b, unsigned char& a)
	{
		int i = (u + v * _width) * 4;
		r = _data[i + 0];
		g = _data[i + 1];
		b = _data[i + 2];
		a = _data[i + 3];
	}
	void Bitmap::rotate()
	{
		unsigned char* buffer = new unsigned char[_width * _height * 4];

		for (unsigned int x = 0; x < _width; x++)
		{
			for (unsigned int y = 0; y < _height; y++)
			{
				buffer[(x * _height + (_height - y - 1)) * 4 + 0] = _data[(y * _width + x) * 4 + 0];
				buffer[(x * _height + (_height - y - 1)) * 4 + 1] = _data[(y * _width + x) * 4 + 1];
				buffer[(x * _height + (_height - y - 1)) * 4 + 2] = _data[(y * _width + x) * 4 + 2];
				buffer[(x * _height + (_height - y - 1)) * 4 + 3] = _data[(y * _width + x) * 4 + 3];
			}
		}

		delete[] _data;
		_data = buffer;
		std::swap(_width, _height);
	}
}