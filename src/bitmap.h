#ifndef BITMAP_H
#define BITMAP_H

namespace Graphics
{
	struct Bitmap
	{
		const unsigned char* _data;
		int _width, _height;

		Bitmap(int width, int height, unsigned char* data);
		Bitmap(const Bitmap& webP);
		~Bitmap();

		void get(int u, int v, unsigned char& r, unsigned char& g, unsigned char& b, unsigned char& a);
		void rotate();
	};
}

#endif