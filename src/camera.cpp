#include "camera.h"

namespace Graphics
{
	Camera::Camera(utility::Vector3f position, utility::Quaternion rotation, float near, float far)
		: position(position), rotation(rotation), near(near), far(far)
	{}

	PerspectiveCamera::PerspectiveCamera(utility::Vector3f position, utility::Quaternion rotation, float fov, float aspect, float near, float far)
		: Camera(position, rotation, near, far), fov(fov), aspect(aspect)
	{}

	OrthoCamera::OrthoCamera(utility::Vector3f position, utility::Quaternion rotation, float left, float bottom, float near, float far, float right, float top)
		: Camera(position, rotation, near, far), left(left), bottom(bottom), right(right), top(top)
	{}

	Matrix Camera::getMatrix()
	{
		Matrix m;
		m  = Matrix::rotate(rotation.conjugate());
		m *= Matrix::translate(-position);
		return m;
	}
	utility::Vector4f Camera::getNormal()
	{
		return utility::Vector4f(-(rotation * utility::Vector3f(0.0f, 0.0f, 1.0f)), 0.0f);
	}

	Matrix PerspectiveCamera::getMatrix()
	{
		return Matrix::perspective(fov, aspect, near, far) * Camera::getMatrix();
	}
	utility::Vector4f PerspectiveCamera::getNormal()
	{
		return utility::Vector4f(position, 1.0f);
	}

	Matrix OrthoCamera::getMatrix()
	{
		return Matrix::ortho(left, bottom, near, far, right, top) * Camera::getMatrix();
	}
}

