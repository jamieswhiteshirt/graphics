#ifndef CAMERA_H
#define CAMERA_H

#include "vertex.h"
#include "matrix.h"

namespace Graphics
{
	class Camera
	{
	public:
		Camera(utility::Vector3f position = utility::Vector3f(), utility::Quaternion rotation = utility::Quaternion(), float near = 1.0f, float far = 10.0f);
		virtual Matrix getMatrix();
		virtual utility::Vector4f getNormal();
		utility::Vector3f position;
		utility::Quaternion rotation;
		float near;
		float far;
	};

	class PerspectiveCamera : public Camera
	{
	public:
		PerspectiveCamera(utility::Vector3f position = utility::Vector3f(), utility::Quaternion rotation = utility::Quaternion(), float fov = 90.0f, float aspect = 1.0f, float near = 1.0f, float far = 10.0f);
		virtual Matrix getMatrix();
		virtual utility::Vector4f getNormal();
		float fov;
		float aspect;
	};

	class OrthoCamera : public Camera
	{
	public:
		OrthoCamera(utility::Vector3f position, utility::Quaternion rotation, float left, float bottom, float near, float far, float right, float top);
		virtual Matrix getMatrix();
		float left;
		float bottom;
		float right;
		float top;
	};
}

#endif
