#include "font.h"
#include <memory>
#include <stdexcept>
#include <string>
#include <string.h>
#include FT_BITMAP_H

#include <iostream>

namespace Graphics
{
	namespace utf8
	{

		typedef unsigned char   uint8_t;
		typedef unsigned short  uint16_t;
		typedef unsigned int    uint32_t;

		namespace internal
		{
			template<typename octet_type>
			inline uint8_t mask8(octet_type oc)
			{
				return static_cast<uint8_t>(0xff & oc);
			}
			template <typename octet_iterator>
			inline typename std::iterator_traits<octet_iterator>::difference_type
			sequence_length(octet_iterator lead_it)
			{
				uint8_t lead = internal::mask8(*lead_it);
				if (lead < 0x80)
					return 1;
				else if ((lead >> 5) == 0x6)
					return 2;
				else if ((lead >> 4) == 0xe)
					return 3;
				else if ((lead >> 3) == 0x1e)
					return 4;
				else
					return 0;
			}
		}

		template <typename octet_iterator>
		uint32_t next(octet_iterator& it)
		{
			uint32_t cp = internal::mask8(*it);
			typename std::iterator_traits<octet_iterator>::difference_type length = internal::sequence_length(it);
			switch (length) {
				case 1:
					break;
				case 2:
					it++;
					cp = ((cp << 6) & 0x7ff) + ((*it) & 0x3f);
					break;
				case 3:
					++it;
					cp = ((cp << 12) & 0xffff) + ((internal::mask8(*it) << 6) & 0xfff);
					++it;
					cp += (*it) & 0x3f;
					break;
				case 4:
					++it;
					cp = ((cp << 18) & 0x1fffff) + ((internal::mask8(*it) << 12) & 0x3ffff);
					++it;
					cp += (internal::mask8(*it) << 6) & 0xfff;
					++it;
					cp += (*it) & 0x3f;
					break;
			}
			++it;
			return cp;
		}
	}
	struct ft_library
	{
		ft_library()
		{
			unsigned int error = FT_Init_FreeType(&library);
			if (error) throw std::runtime_error("FreeType initialization failed (" + std::to_string(error) + ")");
		}
		~ft_library()
		{
			unsigned int error = FT_Done_FreeType(library);
			if (error) throw std::runtime_error("FreeType destruction failed (" + std::to_string(error) + ")");
		}

		FT_Library library;
	};

	static std::unique_ptr<ft_library> lib = nullptr;

	TextInstance::TextInstance(const std::string& text, unsigned int size, Font* font, int wrapWidth, TextAlign align)
		: str(text), _size(size), _wrapWidth(wrapWidth), _atlas(font->_getAtlas(size)), _vertexStream(nullptr), _font(font), _align(align), _width(0), _height(0), lastResizeSequenceNumber(0)
	{
		std::cout << "atlas: "<< _atlas << std::endl;
	}

	float TextInstance::textureWidth() {
		return (float)_atlas->width();
	}

	float TextInstance::textureHeight() {
		return (float)_atlas->height();
	}

	void TextInstance::render()
	{
		_update();
		_atlas->bind();
		_vertexStream->render();
	}

	void TextInstance::_update()
	{
		const int DPI = 64;
		const float scale = 1.0f / DPI;

		if (((MaxRectsTiledTexture<unsigned int>*)_atlas)->resizeSequenceNumber != lastResizeSequenceNumber)
		{
			delete _vertexStream;
			_vertexStream = nullptr;
		}

		if (_vertexStream == nullptr)
		{
			if (_vertexStream) delete _vertexStream;
			_vertexStream = new VertexStream(true);
			_attributeDataStream = std::shared_ptr<AttributeDataStream<VertexUVRGBA>>(new AttributeDataStream<VertexUVRGBA>());
			_vertexStream->addAttributeDataStream(_attributeDataStream);

			unsigned int error = FT_Set_Char_Size(_font->_face, _size * 64, _size * 64, DPI, DPI);
			if (error) throw std::runtime_error("FreeType font loading failed (" + std::to_string(error) + ")");

			_lineHeight = _font->_face->size->metrics.height;

			std::vector<std::pair<std::shared_ptr<TiledTexture<unsigned int>::Icon>, std::pair<float, float>>> glyphs;

			this->_width = 0;

			float x = 0.0f;
			float y = 0.0f;
			int wordBegin = 0;
			for (std::string::const_iterator it = str.begin(); it != str.end();)
			{
				unsigned int c = utf8::next(it);
				if (c == '\n')
				{
					if (x > this->_width) this->_width = x;
					y -= (float)_size;
					x = 0.0f;
					wordBegin = glyphs.size() - 1;
					continue;
				}
				if (c <= 31) continue;
				std::shared_ptr<TiledTexture<unsigned int>::Icon> icon = _font->_getIcon(c, *(MaxRectsTiledTexture<unsigned int>*)_atlas);
				Glyph* g = (Glyph*)icon->tag;


				glyphs.push_back(std::pair<std::shared_ptr<TiledTexture<unsigned int>::Icon>, std::pair<float, float>>(icon, std::pair<float, float>(x, y)));

				x += g->horiAdvance * scale;

				if (c == ' ')
				{
					wordBegin = glyphs.size() - 1;
				}

				if(_wrapWidth > -1 && x + g->width * scale > _wrapWidth)
				{
					if (x > this->_width) this->_width = x;
					y -= _lineHeight;
					x = 0.0f;
					if(wordBegin >= 0)
					{
						for(; wordBegin < glyphs.size(); wordBegin++)
						{
							Glyph* g = (Glyph*)glyphs[wordBegin].first->tag;
							glyphs[wordBegin].second.first = x;
							glyphs[wordBegin].second.second = y;
							x += g->horiAdvance * scale;
						}
					}
				}
			}

			if (x > this->_width) this->_width = x;
			this->_height = -y + _size;

			int index = 0;
			float xoffset = (_align == TA_Center ? x / 2.0f : 0.0f);
			for(auto iter = glyphs.begin(); iter != glyphs.end(); iter++)
			{
				std::shared_ptr<TiledTexture<unsigned int>::Icon> icon = iter->first;
				Glyph* g = (Glyph*)icon->tag;

				float x0 = -xoffset + iter->second.first + g->horiBearingX * scale;
				float y0 = iter->second.second + g->horiBearingY * scale;

				float x1 = x0 + g->width * scale;
				float y1 = y0 - g->height * scale;

				*_attributeDataStream << VertexUVRGBA(x0, y0, 0.0f, icon->getUV(0.0f, 1.0f), 1.0f, 1.0f, 1.0f, 1.0f);
				*_attributeDataStream << VertexUVRGBA(x1, y0, 0.0f, icon->getUV(1.0f, 1.0f), 1.0f, 1.0f, 1.0f, 1.0f);
				*_attributeDataStream << VertexUVRGBA(x1, y1, 0.0f, icon->getUV(1.0f, 0.0f), 1.0f, 1.0f, 1.0f, 1.0f);
				*_attributeDataStream << VertexUVRGBA(x0, y1, 0.0f, icon->getUV(0.0f, 0.0f), 1.0f, 1.0f, 1.0f, 1.0f);

				*_vertexStream << index + 0;
				*_vertexStream << index + 1;
				*_vertexStream << index + 2;
				*_vertexStream << index + 2;
				*_vertexStream << index + 3;
				*_vertexStream << index + 0;

				index += 4;
			}
		}
	}

	float TextInstance::width()
	{
		return this->_width;
	}
	float TextInstance::height()
	{
		return this->_height;
	}
	float TextInstance::lineHeight()
	{
		return this->_lineHeight / 64;
	}

	Font::Font(std::string file)
		: _data(nullptr)
	{
		if (lib == nullptr) lib = std::unique_ptr<ft_library>(new ft_library());
		unsigned int error = FT_New_Face(lib->library, file.c_str(), 0, &_face);
		if (error) throw std::runtime_error("FreeType font loading failed (" + std::to_string(error) + ")");
		FT_Bitmap_New(&_tmpBmp);
	}

	Font::Font(const void* data, size_t size)
		: _data(new FT_Byte[size])
	{
		memcpy((void*)_data, data, size);

		if (lib == nullptr) lib = std::unique_ptr<ft_library>(new ft_library());
		unsigned int error = FT_New_Memory_Face(lib->library, _data, size, 0, &_face);
		if (error) throw std::runtime_error("FreeType font loading failed (" + std::to_string(error) + ")");
		FT_Bitmap_New(&_tmpBmp);
	}

	TextInstance* Font::makeText(const std::string& text, unsigned int size, TextAlign align, int wrapWidth)
	{
		return new TextInstance(text, size, this, wrapWidth, align);
	}

	std::shared_ptr<TiledTexture<unsigned int>::Icon> Font::_getIcon(unsigned int i, MaxRectsTiledTexture<unsigned int>& map)
	{
		std::shared_ptr<TiledTexture<unsigned int>::Icon> res;
		if ((res = map.getIcon(i)) == nullptr)
		{
			unsigned int error = FT_Select_Charmap(_face, FT_ENCODING_UNICODE);
			if (error) throw std::runtime_error("FreeType font loading failed (" + std::to_string(error) + "): Select Unicode encoding");
			error = FT_Load_Char(_face, i, FT_LOAD_RENDER);
			if (error) throw std::runtime_error("FreeType font loading failed (" + std::to_string(error) + "): Render glyph");
			error = FT_Bitmap_Convert(lib->library, &_face->glyph->bitmap, &_tmpBmp, 1);
			if (error) throw std::runtime_error("FreeType font loading failed (" + std::to_string(error) + "): Bitmap convert");

			unsigned char* buffer = new unsigned char[_tmpBmp.width * _tmpBmp.rows * 4];
			memset(buffer, 255, _tmpBmp.width * _tmpBmp.rows * 4);

			for (unsigned int i = 0; i < _tmpBmp.width * _tmpBmp.rows; i++)
			{
				buffer[i * 4 + 3] = _tmpBmp.buffer[i];
			}

			Bitmap bitmap (_tmpBmp.width, _tmpBmp.rows, buffer);
			res = map.getIcon(bitmap, i);
			Glyph* g = (Glyph*)(res->tag = new Glyph());
			FT_Glyph_Metrics& metrics = _face->glyph->metrics;

			g->width = metrics.width;
			g->height = metrics.height;

			g->horiBearingX = metrics.horiBearingX;
			g->horiBearingY = metrics.horiBearingY;
			g->horiAdvance  = metrics.horiAdvance;

			g->vertBearingX = metrics.vertBearingX;
			g->vertBearingY = metrics.vertBearingY;
			g->vertAdvance  = metrics.vertAdvance;

			_glyphs.push_back((Glyph*)res->tag);
		}

		return res;
	}

	TiledTexture<unsigned int>* Font::_getAtlas(unsigned int size)
	{
		auto it = _atlas.find(size);
		if (it == _atlas.end())
		{
			MaxRectsTiledTexture<unsigned int>* atlas = new MaxRectsTiledTexture<unsigned int>(size * 4, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, false, true);
			_atlas.insert(std::pair<unsigned int, MaxRectsTiledTexture<unsigned int>*>(size, atlas));
			std::cout << "Created atlas at " << atlas << std::endl;
			return atlas;
		}

		std::cout << this << ":" << it->second << std::endl;

		return it->second;
	}

	Font::~Font()
	{
		unsigned int error = FT_Bitmap_Done(lib->library, &_tmpBmp);
		if (error) throw std::runtime_error("FreeType font destruction failed (" + std::to_string(error) + "): Bitmap done");

		error = FT_Done_Face(_face);

		for (auto it = _glyphs.begin(); it != _glyphs.end(); it++)
		{
			delete *it;
		}

		if (_data) delete[] _data;
	}
}
