#ifndef FONT_H
#define FONT_H

#include "tiledtexture.h"
#include "vertexstream.h"
#include <ft2build.h>
#include FT_FREETYPE_H

namespace Graphics
{
	enum TextAlign
	{
		TA_Left,
		TA_Center,
		TA_Right
	};

	class Font;

	class TextInstance
	{
	public:
		void render();
		float width();
		float height();
		float lineHeight();
		float textureWidth();
		float textureHeight();

		const std::string str;
	private:
		TextInstance(const std::string& text, unsigned int size, Font* font, int wrapWidth, TextAlign align);

		void _update();

		unsigned int _size;
		int _wrapWidth;
		TiledTexture<unsigned int>* _atlas;
		VertexStream* _vertexStream;
		std::shared_ptr<AttributeDataStream<VertexUVRGBA>> _attributeDataStream;
		Font* _font;
		TextAlign _align;
		float _width;
		float _height;
		float _lineHeight;
		unsigned long long lastResizeSequenceNumber;
		friend class Font;
	};

	class Glyph
	{
	public:
		int width;
		int height;

		int horiBearingX;
		int horiBearingY;
		int horiAdvance;

		int vertBearingX;
		int vertBearingY;
		int vertAdvance;
	};

	class Font
	{
	public:
		Font(std::string file);
		Font(const void* data, size_t size);
		~Font();

		TextInstance* makeText(const std::string& text, unsigned int size, TextAlign align = TA_Left, int wrapWidth = -1);

		const float height() const;

	private:
		std::map<unsigned int, MaxRectsTiledTexture<unsigned int>*> _atlas;
		std::map<unsigned int, MaxRectsTiledTexture<unsigned int>*>::iterator _lastUsed;
		FT_Face _face;
		FT_Bitmap _tmpBmp;
		std::vector<Glyph*> _glyphs;
		const FT_Byte* _data;

		std::shared_ptr<TiledTexture<unsigned int>::Icon> _getIcon(unsigned int i, MaxRectsTiledTexture<unsigned int>& map);
		TiledTexture<unsigned int>* _getAtlas(unsigned int size);
		friend class TextInstance;
	};
};

#endif
