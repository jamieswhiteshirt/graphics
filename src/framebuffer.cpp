#include "framebuffer.h"

namespace Graphics
{
	int FrameBuffer::_defaultWidth = 0;
	int FrameBuffer::_defaultHeight = 0;

	void FrameBuffer::_create()
	{
		if(_created) return;

		glGenFramebuffers(1, &_fbo);
		glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
		
		if(_colorAttachments.size() > 0)
		{
			for(auto iter = _colorAttachments.begin(); iter != _colorAttachments.end(); ++iter)
			{
				iter->second->upload();
				glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + iter->first, iter->second->_texture, 0);
			}
		}
		else
		{
			glDrawBuffer(GL_NONE);
		}
		if(_depthAttachment)
		{
			_depthAttachment->upload();
			glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthAttachment->_texture, 0);
		}

		_created = true;
	}
	void FrameBuffer::setDefaultDimensions(int width, int height)
	{
		_defaultWidth = width;
		_defaultHeight = height;
	}
	void FrameBuffer::useDefaultFrameBuffer()
	{
		glViewport(0, 0, _defaultWidth, _defaultHeight);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	FrameBuffer::FrameBuffer(int width, int height)
		: _fbo(0), _created(false), _width(width), _height(height)
	{}
	FrameBuffer::~FrameBuffer()
	{
		if(_created)
		{
			glDeleteFramebuffers(1, &_fbo);
		}
	}
	void FrameBuffer::attachColorTexture(unsigned int index, AbstractTexture2D* texture)
	{
		if(texture->_mipmapped) throw std::runtime_error("FrameBuffer::attachColorTexture: Cannot attach mipmapped texture to FrameBuffer");
		_colorAttachments[index] = texture;
	}
	void FrameBuffer::attachDepthTexture(AbstractTexture2D* texture)
	{
		if(texture->_mipmapped)  throw std::runtime_error("FrameBuffer::attachDepthTexture: Cannot attach mipmapped texture to FrameBuffer");
		_depthAttachment = texture;
	}
	void FrameBuffer::use()
	{
		_create();
		
		glViewport(0, 0, _width, _height);
		glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
	}
}