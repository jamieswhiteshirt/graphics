#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include "texture.h"
#include <GL/glew.h>
#include <map>
#include <memory>

namespace Graphics
{
	class FrameBuffer
	{
	private:
		GLuint _fbo;
		bool _created;
		std::map<unsigned int, AbstractTexture2D*> _colorAttachments;
		AbstractTexture2D* _depthAttachment;
		int _width, _height;
		static int _defaultWidth, _defaultHeight;

		void _create();
	public:
		static void setDefaultDimensions(int width, int height);
		static void useDefaultFrameBuffer();

		FrameBuffer(int width, int height);
		~FrameBuffer();

		void attachColorTexture(unsigned int index, AbstractTexture2D* texture);
		void attachDepthTexture(AbstractTexture2D* texture);
		void use();
	};
}

#endif
