#include <GL/glew.h>
#include "graphicserror.h"
#include <stdexcept>

namespace Graphics
{
	void ifErrorThrow(std::string errorCaption)
	{
		GLenum error = glGetError();
		if(error != GL_NO_ERROR)
		{
			throw std::runtime_error(("GL ERROR: " + errorCaption + " - " + std::to_string(error)).c_str());
		}
	}
}
