#ifndef ERROR_H
#define ERROR_H

#include <string>
#include <string.h>

namespace Graphics
{
	extern void ifErrorThrow(std::string errorCaption);
}

#endif