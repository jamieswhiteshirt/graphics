#define _USE_MATH_DEFINES

#include "matrix.h"
#include <math.h>
#include <string.h>

namespace Graphics
{
	void Matrix::_copy(const float* data)
	{
		for(int i = 0; i < 16; i++)
		{
			_data[i] = data[i];
		}
	}
	Matrix::Matrix()
	{
		for(int i = 0; i < 16; i++)
		{
			_data[i] = 0.0f;
		}
	}
	Matrix::Matrix(float* data)
	{
		_copy(data);
	}
	Matrix::Matrix(Matrix3& mat)
	{
		float matData[] = {
			mat(0, 0), mat(1, 0), mat(2, 0), 0.0f,
			mat(0, 1), mat(1, 1), mat(2, 1), 0.0f,
			mat(0, 2), mat(1, 2), mat(2, 2), 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		};
		_copy(matData);
	}
	const float& Matrix::operator()(int x, int y) const
	{
		return _data[x + y * 4];
	}
	float& Matrix::operator()(int x, int y)
	{
		return _data[x + y * 4];
	}
	Matrix Matrix::operator*(const Matrix& matrix) const
	{
		Matrix result;

		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				float value = 0.0f;
				for(int k = 0; k < 4; k++)
				{
					value += _data[k + j * 4] * matrix(i, k);
				}
				result(i, j) = value;
			}
		}

		return result;
	}
	Matrix& Matrix::operator*=(const Matrix& matrix)
	{
		Matrix result = *this * matrix;
		_copy(result._data);
		return *this;
	}
	Matrix Matrix::operator*(const float& scalar) const
	{
		Matrix result;

		for(int i = 0; i < 16; i++)
		{
			result._data[i] = _data[i] * scalar;
		}

		return result;
	}
	Matrix& Matrix::operator*=(const float& scalar)
	{
		Matrix result = *this * scalar;
		_copy(result._data);
		return *this;
	}
	Matrix& Matrix::operator=(const Matrix& mat)
	{
		_copy(mat._data);

		return *this;
	}
	bool Matrix::operator==(const Matrix& matrix) const
	{
		for(int i = 0; i < 16; i++)
		{
			if(_data[i] != matrix._data[i]) return false;
		}
		return true;
	}
	bool Matrix::operator!=(const Matrix& matrix) const
	{
		return !operator==(matrix);
	}
	Matrix Matrix::inversed() const
	{
		Matrix res;

		res._data[0] = _data[5]  * _data[10] * _data[15] - 
			_data[5]  * _data[11] * _data[14] - 
			_data[9]  * _data[6]  * _data[15] + 
			_data[9]  * _data[7]  * _data[14] +
			_data[13] * _data[6]  * _data[11] - 
			_data[13] * _data[7]  * _data[10];

		res._data[4] = -_data[4]  * _data[10] * _data[15] + 
			_data[4]  * _data[11] * _data[14] + 
			_data[8]  * _data[6]  * _data[15] - 
			_data[8]  * _data[7]  * _data[14] - 
			_data[12] * _data[6]  * _data[11] + 
			_data[12] * _data[7]  * _data[10];

		res._data[8] = _data[4]  * _data[9] * _data[15] - 
			_data[4]  * _data[11] * _data[13] - 
			_data[8]  * _data[5] * _data[15] + 
			_data[8]  * _data[7] * _data[13] + 
			_data[12] * _data[5] * _data[11] - 
			_data[12] * _data[7] * _data[9];

		res._data[12] = -_data[4]  * _data[9] * _data[14] + 
			_data[4]  * _data[10] * _data[13] +
			_data[8]  * _data[5] * _data[14] - 
			_data[8]  * _data[6] * _data[13] - 
			_data[12] * _data[5] * _data[10] + 
			_data[12] * _data[6] * _data[9];

		res._data[1] = -_data[1]  * _data[10] * _data[15] + 
			_data[1]  * _data[11] * _data[14] + 
			_data[9]  * _data[2] * _data[15] - 
			_data[9]  * _data[3] * _data[14] - 
			_data[13] * _data[2] * _data[11] + 
			_data[13] * _data[3] * _data[10];

		res._data[5] = _data[0]  * _data[10] * _data[15] - 
			_data[0]  * _data[11] * _data[14] - 
			_data[8]  * _data[2] * _data[15] + 
			_data[8]  * _data[3] * _data[14] + 
			_data[12] * _data[2] * _data[11] - 
			_data[12] * _data[3] * _data[10];

		res._data[9] = -_data[0]  * _data[9] * _data[15] + 
			_data[0]  * _data[11] * _data[13] + 
			_data[8]  * _data[1] * _data[15] - 
			_data[8]  * _data[3] * _data[13] - 
			_data[12] * _data[1] * _data[11] + 
			_data[12] * _data[3] * _data[9];

		res._data[13] = _data[0]  * _data[9] * _data[14] - 
			_data[0]  * _data[10] * _data[13] - 
			_data[8]  * _data[1] * _data[14] + 
			_data[8]  * _data[2] * _data[13] + 
			_data[12] * _data[1] * _data[10] - 
			_data[12] * _data[2] * _data[9];

		res._data[2] = _data[1]  * _data[6] * _data[15] - 
			_data[1]  * _data[7] * _data[14] - 
			_data[5]  * _data[2] * _data[15] + 
			_data[5]  * _data[3] * _data[14] + 
			_data[13] * _data[2] * _data[7] - 
			_data[13] * _data[3] * _data[6];

		res._data[6] = -_data[0]  * _data[6] * _data[15] + 
			_data[0]  * _data[7] * _data[14] + 
			_data[4]  * _data[2] * _data[15] - 
			_data[4]  * _data[3] * _data[14] - 
			_data[12] * _data[2] * _data[7] + 
			_data[12] * _data[3] * _data[6];

		res._data[10] = _data[0]  * _data[5] * _data[15] - 
			_data[0]  * _data[7] * _data[13] - 
			_data[4]  * _data[1] * _data[15] + 
			_data[4]  * _data[3] * _data[13] + 
			_data[12] * _data[1] * _data[7] - 
			_data[12] * _data[3] * _data[5];

		res._data[14] = -_data[0]  * _data[5] * _data[14] + 
			_data[0]  * _data[6] * _data[13] + 
			_data[4]  * _data[1] * _data[14] - 
			_data[4]  * _data[2] * _data[13] - 
			_data[12] * _data[1] * _data[6] + 
			_data[12] * _data[2] * _data[5];

		res._data[3] = -_data[1] * _data[6] * _data[11] + 
			_data[1] * _data[7] * _data[10] + 
			_data[5] * _data[2] * _data[11] - 
			_data[5] * _data[3] * _data[10] - 
			_data[9] * _data[2] * _data[7] + 
			_data[9] * _data[3] * _data[6];

		res._data[7] = _data[0] * _data[6] * _data[11] - 
			_data[0] * _data[7] * _data[10] - 
			_data[4] * _data[2] * _data[11] + 
			_data[4] * _data[3] * _data[10] + 
			_data[8] * _data[2] * _data[7] - 
			_data[8] * _data[3] * _data[6];

		res._data[11] = -_data[0] * _data[5] * _data[11] + 
			_data[0] * _data[7] * _data[9] + 
			_data[4] * _data[1] * _data[11] - 
			_data[4] * _data[3] * _data[9] - 
			_data[8] * _data[1] * _data[7] + 
			_data[8] * _data[3] * _data[5];

		res._data[15] = _data[0] * _data[5] * _data[10] - 
			_data[0] * _data[6] * _data[9] - 
			_data[4] * _data[1] * _data[10] + 
			_data[4] * _data[2] * _data[9] + 
			_data[8] * _data[1] * _data[6] - 
			_data[8] * _data[2] * _data[5];

		float idet = 1.0f / (_data[0] * res._data[0] + _data[1] * res._data[4] + _data[2] * res._data[8] + _data[3] * res._data[12]);

		for(int i = 0; i < 4; i++)
		{
			res._data[i] *= idet;
		}

		return res;
	}
	Matrix Matrix::transposed() const
	{
		float matData[] = {
			_data[0], _data[4], _data[8], _data[12],
			_data[1], _data[5], _data[9], _data[13],
			_data[2], _data[6], _data[10], _data[14],
			_data[3], _data[7], _data[11], _data[15]
		};
		return Matrix(matData);
	}
	float Matrix::determinant() const
	{
		float det0 = 0.0f;

		for(int x0 = 0; x0 < 4; x0++)
		{
			float det1 = 0.0f;

			int x0a[] = {
				x0 > 0 ? 0 : 1,
				x0 > 1 ? 1 : 2,
				x0 > 2 ? 2 : 3
			};

			for(int x1 = 0; x1 < 3; x1++)
			{
				int x1a[] = {
					x0a[x1 > 0 ? 0 : 1],
					x0a[x1 > 1 ? 1 : 2]
				};

				float det2 = _data[x1a[0] + 8] * _data[x1a[1] + 12] - _data[x1a[1] + 8] * _data[x1a[0] + 12];

				if((x1 & 1) == 0) det1 += _data[x0a[x1] + 4] * det2;
				else det1 -= _data[x0a[x1] + 4] * det2;
			}

			if((x0 & 1) == 0) det0 += det1 * _data[x0];
			else det0 -= det1 * _data[x0];
		}

		return det0;
	}
	utility::Vector3f Matrix::transform3(const utility::Vector3f& v, bool divideByW) const
	{
		utility::Vector3f res(v._x * _data[0] + v._y * _data[1] + v._z * _data[2] + _data[3],
			v._x * _data[4] + v._y * _data[5] + v._z * _data[6] + _data[7],
			v._x * _data[8] + v._y * _data[9] + v._z * _data[10] + _data[11]);
		if(divideByW)
		{
			return res / getW(v);
		}
		else
		{
			return res;
		}
	}
	utility::Vector4f Matrix::transform4(const utility::Vector3f& v) const
	{
		return utility::Vector4f(v._x * _data[0] + v._y * _data[1] + v._z * _data[2] + _data[3],
			v._x * _data[4] + v._y * _data[5] + v._z * _data[6] + _data[7],
			v._x * _data[8] + v._y * _data[9] + v._z * _data[10] + _data[11],
			v._x * _data[12] + v._y * _data[13] + v._z * _data[14] + _data[15]);
	}
	utility::Vector4f Matrix::transform4(const utility::Vector4f& v) const
	{
		return utility::Vector4f(v._x * _data[0] + v._y * _data[1] + v._z * _data[2] + v._w * _data[3],
			v._x * _data[4] + v._y * _data[5] + v._z * _data[6] + v._w * _data[7],
			v._x * _data[8] + v._y * _data[9] + v._z * _data[10] + v._w * _data[11],
			v._x * _data[12] + v._y * _data[13] + v._z * _data[14] + v._w * _data[15]);
	}
	float Matrix::getW(const utility::Vector3f& v) const
	{
		return v._x * _data[12] + v._y * _data[13] + v._z * _data[14] + _data[15];
	}
	Matrix Matrix::identity()
	{
		Matrix result;
		result(0, 0) = 1.0f;
		result(1, 1) = 1.0f;
		result(2, 2) = 1.0f;
		result(3, 3) = 1.0f;

		return result;
	}
	Matrix Matrix::scale(float x, float y, float z)
	{
		Matrix result;
		result(0, 0) = x;
		result(1, 1) = y;
		result(2, 2) = z;
		result(3, 3) = 1.0f;

		return result;
	}
	Matrix Matrix::scale(utility::Vector3f v)
	{
		return scale(v._x, v._y, v._z);
	}
	Matrix Matrix::translate(float x, float y, float z)
	{
		Matrix result = identity();
		result(3, 0) = x;
		result(3, 1) = y;
		result(3, 2) = z;

		return result;
	}
	Matrix Matrix::translate(utility::Vector3f v)
	{
		return translate(v._x, v._y, v._z);
	}
	Matrix Matrix::ortho2D(float left, float bottom, float right, float top)
	{
		float matData[] = {
			2.0f / (right - left), 0.0f, 0.0f, (right + left) / (left - right),
			0.0f, 2.0f / (top - bottom), 0.0f, (top + bottom) / (bottom - top),
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		};

		return Matrix(matData);
	}
	Matrix Matrix::ortho(float left, float bottom, float n, float right, float top, float f)
	{
		float matData[] = {
			2.0f / (right - left), 0.0f, 0.0f, (right + left) / (left - right),
			0.0f, 2.0f / (top - bottom), 0.0f, (top + bottom) / (bottom - top),
			0.0f, 0.0f, 2.0f / (f - n), (f + n) / (n - f),
			0.0f, 0.0f, 0.0f, 1.0f
		};

		return Matrix(matData);
	}
	Matrix Matrix::rotate(float degrees, float x, float y, float z)
	{
		float s = sinf(degrees * (float)M_PI / 180.0f);
		float c = cosf(degrees * (float)M_PI / 180.0f);

		float matData[] = {
			x + (y + z) * c, z * -s, y * s, 0.0f,
			z * s, y + (x + z) * c, x * -s, 0.0f,
			y * -s, x * s, z + (x + y) * c, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		};

		return Matrix(matData);
	}
	Matrix Matrix::rotate(utility::Quaternion r)
	{
		float xx = r._x * r._x;
		float xy = r._x * r._y;
		float xz = r._x * r._z;
		float xw = r._x * r._w;
		float yy = r._y * r._y;
		float yz = r._y * r._z;
		float yw = r._y * r._w;
		float zz = r._z * r._z;
		float zw = r._z * r._w;

		float matData[] = {
			1.0f - 2.0f * (yy + zz), 2.0f * (xy - zw), 2.0f * (xz + yw), 0.0f,
			2.0f * (xy + zw), 1.0f - 2.0f * (xx + zz), 2.0f * (yz - xw), 0.0f,
			2.0f * (xz - yw), 2.0f * (yz + xw), 1.0f - 2.0f * (xx + yy), 0.0f,
			            0.0f,             0.0f,                    0.0f, 1.0f
		};

		return Matrix(matData);
	}
	Matrix Matrix::perspective(float fov, float aspect, float n, float f)
	{
		float frustumDepth = f - n;
		float oneOverDepth = 1 / frustumDepth;

		float matData[] = {
			(1.0f / (float)tan(fov * M_PI / 360.0f)) / aspect, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f / (float)tan(fov * M_PI / 360.0f), 0.0f, 0.0f,
			0.0f, 0.0f, f * oneOverDepth, -f * n * oneOverDepth,
			0.0f, 0.0f, 1.0f, 0.0f
		};

		return Matrix(matData);
	}
	
	void Matrix3::_copy(const float* data)
	{
		for(int i = 0; i < 9; i++)
		{
			_data[i] = data[i];
		}
	}
	Matrix3::Matrix3()
	{
		for(int i = 0; i < 9; i++)
		{
			_data[i] = 0.0f;
		}
	}
	Matrix3::Matrix3(float* data)
	{
		_copy(data);
	}
	Matrix3::Matrix3(Matrix mat)
	{
		float matData[] = {
			mat(0, 0), mat(1, 0), mat(2, 0),
			mat(0, 1), mat(1, 1), mat(2, 1),
			mat(0, 2), mat(1, 2), mat(2, 2)
		};
		_copy(matData);
	}
	const float& Matrix3::operator()(int x, int y) const
	{
		return _data[x + y * 3];
	}
	float& Matrix3::operator()(int x, int y)
	{
		return _data[x + y * 3];
	}
	Matrix3 Matrix3::operator*(const Matrix3& matrix) const
	{
		Matrix3 result;

		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				float value = 0.0f;
				for(int k = 0; k < 3; k++)
				{
					value += _data[k + j * 3] * matrix(i, k);
				}
				result(i, j) = value;
			}
		}

		return result;
	}
	Matrix3& Matrix3::operator*=(const Matrix3& matrix)
	{
		Matrix3 result = *this * matrix;
		_copy(result._data);
		return *this;
	}
	Matrix3 Matrix3::operator*(const float& scalar) const
	{
		Matrix3 result;

		for(int i = 0; i < 9; i++)
		{
			result._data[i] = _data[i] * scalar;
		}

		return result;
	}
	Matrix3& Matrix3::operator*=(const float& scalar)
	{
		Matrix3 result = *this * scalar;
		_copy(result._data);
		return *this;
	}
	Matrix3& Matrix3::operator=(const Matrix3& mat)
	{
		_copy(mat._data);
		return *this;
	}
	bool Matrix3::operator==(const Matrix3& matrix) const
	{
		for(int i = 0; i < 9; i++)
		{
			if(_data[i] != matrix._data[i]) return false;
		}
		return true;
	}
	bool Matrix3::operator!=(const Matrix3& matrix) const
	{
		return !operator==(matrix);
	}
	Matrix3 Matrix3::inversed() const
	{
		Matrix3 res;

		res._data[0] = _data[4] * _data[8] - _data[5] * _data[7];
		res._data[3] = _data[3] * _data[8] - _data[5] * _data[6];
		res._data[6] = _data[3] * _data[7] - _data[4] * _data[6];
		res._data[1] = _data[1] * _data[8] - _data[2] * _data[7];
		res._data[4] = _data[0] * _data[8] - _data[2] * _data[6];
		res._data[7] = _data[0] * _data[7] - _data[1] * _data[6];
		res._data[2] = _data[1] * _data[5] - _data[2] * _data[4];
		res._data[5] = _data[0] * _data[5] - _data[2] * _data[3];
		res._data[8] = _data[0] * _data[4] - _data[1] * _data[3];

		float idet = 1.0f / (_data[0] * res._data[0] + _data[1] * res._data[3] + _data[2] * res._data[6]);

		for(int i = 0; i < 9; i++)
		{
			res._data[i] *= idet;
		}

		return res;
	}
	Matrix3 Matrix3::transposed() const
	{
		float matData[] = {
			_data[0], _data[3], _data[6],
			_data[1], _data[4], _data[7],
			_data[2], _data[5], _data[8]
		};
		return Matrix3(matData);
	}
	float Matrix3::determinant() const
	{
		float det0 = 0.0f;
		for(int x0 = 0; x0 < 3; x0++)
		{
			int x0a[] = {
				x0 > 0 ? 0 : 1,
				x0 > 1 ? 1 : 2
			};

			float det1 = _data[x0a[0] + 3] * _data[x0a[1] + 6] - _data[x0a[1] + 3] * _data[x0a[0] + 6];

			det0 += det1 * ((x0 & 1) == 0 ? _data[x0] : -_data[x0]);
		}
		return det0;
	}
	Matrix3 Matrix3::identity()
	{
		Matrix3 result;
		result(0, 0) = 1.0f;
		result(1, 1) = 1.0f;
		result(2, 2) = 1.0f;

		return result;
	}
	Matrix3 Matrix3::scale(float x, float y, float z)
	{
		Matrix3 result;
		result(0, 0) = x;
		result(1, 1) = y;
		result(2, 2) = z;

		return result;
	}
	Matrix3 Matrix3::scale(utility::Vector3f s)
	{
		return scale(s._x, s._y, s._z);
	}
	Matrix3 Matrix3::rotate(float degrees, float x, float y, float z)
	{
		float s = (float)sin(degrees * M_PI / 180.0f);
		float c = (float)cos(degrees * M_PI / 180.0f);

		float matData[] = {
			x + (y + z) * c, z * -s, y * s,
			z * s, y + (x + z) * c, x * -s,
			y * -s, x * s, z + (x + y) * c
		};

		return Matrix3(matData);
	}
	Matrix3 Matrix3::rotate(utility::Quaternion r)
	{
		float xx = r._x * r._x;
		float xy = r._x * r._y;
		float xz = r._x * r._z;
		float xw = r._x * r._w;
		float yy = r._y * r._y;
		float yz = r._y * r._z;
		float yw = r._y * r._w;
		float zz = r._z * r._z;
		float zw = r._z * r._w;

		float matData[] = {
			1.0f - 2.0f * (yy + zz), 2.0f * (xy - zw), 2.0f * (xz + yw),
			2.0f * (xy + zw), 1.0f - 2.0f * (xx + zz), 2.0f * (yz - xw),
			2.0f * (xz - yw), 2.0f * (yz + xw), 1.0f - 2.0f * (xx + yy)
		};

		return Matrix3(matData);
	}
}