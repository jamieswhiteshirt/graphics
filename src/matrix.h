#ifndef MATRIX_H
#define MATRIX_H

#include "vertex.h"
#include <vector.h>

namespace Graphics
{
	class Matrix3;

	class Matrix
	{
	private:
		float _data[16];
		void _copy(const float* data);
	public:
		Matrix();
		Matrix(float* data);
		Matrix(Matrix3& mat);
		const float& operator()(int x, int y) const;
		float& operator()(int x, int y);
		Matrix operator*(const Matrix& matrix) const;
		Matrix& operator*=(const Matrix& matrix);
		Matrix operator*(const float& scalar) const;
		Matrix& operator*=(const float& scalar);
		Matrix& operator=(const Matrix& matrix);
		bool operator==(const Matrix& matrix) const;
		bool operator!=(const Matrix& matrix) const;

		Matrix inversed() const;
		Matrix transposed() const;
		float determinant() const;
		utility::Vector3f transform3(const utility::Vector3f& v, bool divideByW) const;
		utility::Vector4f transform4(const utility::Vector3f& v) const;
		utility::Vector4f transform4(const utility::Vector4f& v) const;
		float getW(const utility::Vector3f& v) const;

		static Matrix identity();
		static Matrix scale(float x = 1.0f, float y = 1.0f, float z = 1.0f);
		static Matrix scale(utility::Vector3f v);
		static Matrix translate(float x = 0.0f, float y = 0.0f, float z = 0.0f);
		static Matrix translate(utility::Vector3f v);
		static Matrix ortho2D(float left, float bottom, float right, float top);
		static Matrix ortho(float left, float bottom, float near, float right, float top, float far);
		static Matrix rotate(float degrees, float x, float y, float z);
		static Matrix rotate(utility::Quaternion r);
		static Matrix perspective(float fov, float aspect, float n, float f);
	};

	class Matrix3
	{
	private:
		float _data[9];
		void _copy(const float* data);
	public:
		Matrix3();
		Matrix3(float* data);
		Matrix3(Matrix mat);
		const float& operator()(int x, int y) const;
		float& operator()(int x, int y);
		Matrix3 operator*(const Matrix3& matrix) const;
		Matrix3& operator*=(const Matrix3& matrix);
		Matrix3 operator*(const float& scalar) const;
		Matrix3& operator*=(const float& scalar);
		Matrix3& operator=(const Matrix3& matrix);
		bool operator==(const Matrix3& matrix) const;
		bool operator!=(const Matrix3& matrix) const;

		Matrix3 inversed() const;
		Matrix3 transposed() const;
		float determinant() const;

		static Matrix3 identity();
		static Matrix3 scale(float x = 1.0f, float y = 1.0f, float z = 1.0f);
		static Matrix3 scale(utility::Vector3f s);
		static Matrix3 rotate(float degrees, float x, float y, float z);
		static Matrix3 rotate(utility::Quaternion r);
	};
}

#endif