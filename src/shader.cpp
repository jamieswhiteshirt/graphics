#include "shader.h"
#include <sstream>
#include <ostream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <strutil.h>
#include <metaexception.h>

namespace Graphics
{
	template<typename datatype>
	void MatrixStack<datatype>::_updateUniforms()
	{
		for(auto iter = _matrixUniforms.begin(); iter != _matrixUniforms.end(); ++iter)
		{
			_uploadUniform(*iter);
		}
	}
	template<>
	void MatrixStack<Matrix>::_uploadUniform(std::shared_ptr<Uniform> uniform)
	{
		uniform->setm(_computedStack.top());
	}
	template<>
	void MatrixStack<Matrix3>::_uploadUniform(std::shared_ptr<Uniform> uniform)
	{
		uniform->setm3(_computedStack.top());
	}
	template<typename datatype>
	MatrixStack<datatype>::MatrixStack()
	{
		clear();
	}
	template<typename datatype>
	void MatrixStack<datatype>::push(datatype matrix)
	{
		_computedStack.push(_computedStack.top() * matrix);
		_updateUniforms();
	}
	template<typename datatype>
	void MatrixStack<datatype>::mult(datatype matrix)
	{
		_computedStack.top() *= matrix;
		_updateUniforms();
	}
	template<typename datatype>
	void MatrixStack<datatype>::identity()
	{
		_computedStack.pop();
		_computedStack.push(_computedStack.top());
		_updateUniforms();
	}
	template<typename datatype>
	void MatrixStack<datatype>::pop()
	{
		_computedStack.pop();
		_updateUniforms();
	}
	template<typename datatype>
	void MatrixStack<datatype>::clear()
	{
		while(!_computedStack.empty())
		{
			_computedStack.pop();
		}
		_computedStack.push(datatype::identity());
		_updateUniforms();
	}
	template<typename datatype>
	const datatype MatrixStack<datatype>::get()
	{
		return _computedStack.top();
	}
	template<typename datatype>
	void MatrixStack<datatype>::bindMatrixUniform(std::shared_ptr<Uniform> uniform)
	{
		_matrixUniforms.emplace(uniform);
		_updateUniforms();
	}
	template<typename datatype>
	void MatrixStack<datatype>::unbindMatrixUniform(std::shared_ptr<Uniform> uniform)
	{
		_matrixUniforms.erase(uniform);
	}
	template<typename datatype>
	void MatrixStack<datatype>::unbindAllMatrixUniforms()
	{
		_matrixUniforms.clear();
	}

	template class MatrixStack<Matrix>;
	template class MatrixStack<Matrix3>;

	Shader::Shader(std::string name, std::string content, std::map<std::string, std::string> params, GLenum type)
		: name(name), _shader(0), _type(type), _compiled(false)
	{
		std::istringstream strIn(content);
		std::ostringstream strOut;

		const unsigned int maxNesting = 16;
		unsigned int nestingLevel = 0;
		unsigned int blockingNestingLevel = maxNesting;
		bool chainFulfilled[maxNesting + 1];
		for(int i = 0; i < maxNesting + 1; i++) chainFulfilled[i] = false;

		for(std::string lineIn; std::getline(strIn, lineIn);)
		{
			std::ostringstream lineOut;
			
			std::string::size_type searchPos = 0;
			do
			{
				std::string::size_type lastSearchPos = searchPos;
				searchPos = lineIn.find("$(", searchPos);
				lineOut << lineIn.substr(lastSearchPos, searchPos);

				if(searchPos != lineIn.npos)
				{
					std::string::size_type beginPos = searchPos + 2;
					searchPos = lineIn.find(')', beginPos) + 1;
					lineOut << params[lineIn.substr(beginPos, searchPos - beginPos - 1)];
				}
			}
			while(searchPos != lineIn.npos);

			std::string trimmedLineIn = utility::trim(lineOut.str());

			if(trimmedLineIn.length() > 0 && trimmedLineIn[0] == '$')
			{
				std::string::size_type paramNamePos = trimmedLineIn.find(' ');
				std::string paramName = paramNamePos != trimmedLineIn.npos ? trimmedLineIn.substr(paramNamePos + 1) : "";

				if(trimmedLineIn.find("$ifdef") != trimmedLineIn.npos)
				{
					nestingLevel++;
					if(blockingNestingLevel > nestingLevel)
					{
						if(params.find(paramName) == params.end())
						{
							blockingNestingLevel = nestingLevel;
						}
						else
						{
							chainFulfilled[nestingLevel] = true;
						}
					}
				}
				else if(trimmedLineIn.find("$ifndef") != trimmedLineIn.npos)
				{
					nestingLevel++;
					if(blockingNestingLevel > nestingLevel)
					{
						if(params.find(paramName) != params.end())
						{
							blockingNestingLevel = nestingLevel;
						}
						else
						{
							chainFulfilled[nestingLevel] = true;
						}
					}
				}
				else if(trimmedLineIn.find("$elifdef") != trimmedLineIn.npos)
				{
					if(!chainFulfilled[nestingLevel] && blockingNestingLevel >= nestingLevel)
					{
						if(params.find(paramName) != params.end())
						{
							blockingNestingLevel = maxNesting;
							chainFulfilled[nestingLevel] = true;
						}
						else
						{
							blockingNestingLevel = nestingLevel;
						}
					}
				}
				else if(trimmedLineIn.find("$elifndef") != trimmedLineIn.npos)
				{
					if(!chainFulfilled[nestingLevel] && blockingNestingLevel >= nestingLevel)
					{
						if(params.find(paramName) == params.end())
						{
							blockingNestingLevel = maxNesting;
							chainFulfilled[nestingLevel] = true;
						}
						else
						{
							blockingNestingLevel = nestingLevel;
						}
					}
				}
				else if(trimmedLineIn.find("$else") != trimmedLineIn.npos)
				{
					if(blockingNestingLevel >= nestingLevel)
					{
						if(!chainFulfilled[nestingLevel])
						{
							blockingNestingLevel = maxNesting;
							chainFulfilled[nestingLevel] = true;
						}
						else
						{
							blockingNestingLevel = nestingLevel;
						}
					}
				}
				else if(trimmedLineIn.find("$endif") != trimmedLineIn.npos)
				{
					nestingLevel--;
					if(blockingNestingLevel > nestingLevel)
					{
						blockingNestingLevel = maxNesting;
						chainFulfilled[nestingLevel + 1] = false;
					}
				}

				strOut << "//" + trimmedLineIn;
			}
			else
			{
				if(blockingNestingLevel <= nestingLevel)
				{
					strOut << "//";
				}
				strOut << trimmedLineIn;
			}


			strOut << "\n";
		}

		_content = strOut.str();
	}
	void Shader::_compile()
	{
		if(_compiled) return;

		const char* strPtrs = _content.c_str();
		const GLint lengths = _content.length();

		_shader = glCreateShader(_type);
		glShaderSource(_shader, 1, &strPtrs, &lengths);
		glCompileShader(_shader);

		GLint compileStatus;

		glGetObjectParameterivARB(_shader, GL_OBJECT_COMPILE_STATUS_ARB, &compileStatus);

		if(compileStatus == GL_FALSE)
		{
			utility::MetaException exception("GL ERROR: Failed to compile shader");

			GLint len;
			glGetShaderiv(_shader, GL_INFO_LOG_LENGTH, &len);
			if(len > 0)
			{
				GLchar* str = new GLchar[len];
				glGetShaderInfoLog(_shader, len, nullptr, str);

				std::string shaderType;
				switch(_type)
				{
				case GL_VERTEX_SHADER:
					shaderType = "Vertex";
					break;
				case GL_GEOMETRY_SHADER:
					shaderType = "Geometry";
					break;
				case GL_FRAGMENT_SHADER:
					shaderType = "Fragment";
					break;
				}

				std::string logString = std::string(str);
				delete[] str;

				exception.addMeta("log", logString);
			}
			else
			{
				exception.addMeta("log", "No log returned");
			}
			
			std::istringstream linesIn;
			std::ostringstream linesOut;
			std::string line;

			for(int lineNum = 1; std::getline(linesIn, line); lineNum++)
			{
				std::string lineNumString = std::to_string(lineNum);
				while(lineNumString.size() < 3)
				{
					lineNumString = " " + lineNumString;
				}

				linesOut << "/*" << lineNumString << "*/" << line << "\n";
			}

			exception.addMeta("src", linesOut.str());
			throw exception;
		}

		_compiled = true;
	}
	Shader::~Shader()
	{
		if(_compiled)
		{
			glDeleteShader(_shader);
		}
	}

	VertexShader::VertexShader(std::string name, std::string content, std::map<std::string, std::string> params)
		: Shader(name, content, params, GL_VERTEX_SHADER)
	{}

	GeometryShader::GeometryShader(std::string name, std::string content, std::map<std::string, std::string> params)
		: Shader(name, content, params, GL_GEOMETRY_SHADER)
	{}

	FragmentShader::FragmentShader(std::string name, std::string content, std::map<std::string, std::string> params)
		: Shader(name, content, params, GL_FRAGMENT_SHADER)
	{}

	std::shared_ptr<Uniform> Uniform::_factory(ShaderProgram* program, std::string name, GLint location, GLint type, GLint arraySize)
	{
		switch(type)
		{
		case GL_FLOAT:
			return std::shared_ptr<Uniform>(new FloatUniform(program, location, name, 1, arraySize));
		case GL_FLOAT_VEC2:
			return std::shared_ptr<Uniform>(new FloatUniform(program, location, name, 2, arraySize));
		case GL_FLOAT_VEC3:
			return std::shared_ptr<Uniform>(new FloatUniform(program, location, name, 3, arraySize));
		case GL_FLOAT_VEC4:
			return std::shared_ptr<Uniform>(new FloatUniform(program, location, name, 4, arraySize));
		case GL_SAMPLER_2D:
		case GL_INT:
			return std::shared_ptr<Uniform>(new IntUniform(program, location, name, 1, arraySize));
		case GL_INT_VEC2:
			return std::shared_ptr<Uniform>(new IntUniform(program, location, name, 2, arraySize));
		case GL_INT_VEC3:
			return std::shared_ptr<Uniform>(new IntUniform(program, location, name, 3, arraySize));
		case GL_INT_VEC4:
			return std::shared_ptr<Uniform>(new IntUniform(program, location, name, 4, arraySize));
		case GL_UNSIGNED_INT:
			return std::shared_ptr<Uniform>(new UintUniform(program, location, name, 1, arraySize));
		case GL_UNSIGNED_INT_VEC2:
			return std::shared_ptr<Uniform>(new UintUniform(program, location, name, 2, arraySize));
		case GL_UNSIGNED_INT_VEC3:
			return std::shared_ptr<Uniform>(new UintUniform(program, location, name, 3, arraySize));
		case GL_UNSIGNED_INT_VEC4:
			return std::shared_ptr<Uniform>(new UintUniform(program, location, name, 4, arraySize));
		case GL_BOOL:
			return std::shared_ptr<Uniform>(new IntUniform(program, location, name, 1, arraySize));
		case GL_BOOL_VEC2:
			return std::shared_ptr<Uniform>(new IntUniform(program, location, name, 2, arraySize));
		case GL_BOOL_VEC3:
			return std::shared_ptr<Uniform>(new IntUniform(program, location, name, 3, arraySize));
		case GL_BOOL_VEC4:
			return std::shared_ptr<Uniform>(new IntUniform(program, location, name, 4, arraySize));
		case GL_FLOAT_MAT4:
			return std::shared_ptr<Uniform>(new MatrixUniform(program, location, name, arraySize));
		case GL_FLOAT_MAT3:
			return std::shared_ptr<Uniform>(new Matrix3Uniform(program, location, name, arraySize));
		default:
			return std::shared_ptr<Uniform>(new Uniform(program, location, name, arraySize));
		}
	}
	Uniform::Uniform(ShaderProgram* program, GLint location, std::string name, GLsizei arraySize)
		: _location(location), _arraySize(arraySize), _modified(true), program(program), name(name)
	{}
	void Uniform::_upload()
	{}
	void Uniform::_defaultFailure()
	{
		utility::MetaException exception("Invalid uniform type");
		exception.addMeta("name", name);
		exception.addMeta("shader program", program->name);
		throw exception;
	}
	Uniform::~Uniform()
	{}

	VecUniform::VecUniform(ShaderProgram* program, GLint location, std::string name, GLsizei size, GLsizei arraySize)
		: Uniform(program, location, name, arraySize), _size(size)
	{}

	FloatUniform::FloatUniform(ShaderProgram* program, GLint location, std::string name, GLsizei size, GLsizei arraySize)
		: VecUniform(program, location, name, size, arraySize), _values(new float[size * arraySize])
	{
		for(int i = 0; i < size * arraySize; i++)
		{
			_values[i] = 0.0f;
		}
	}
	void FloatUniform::_upload()
	{
		if(!_modified) return;
		switch(_size)
		{
		case 1:
			glUniform1fv(_location, _arraySize, _values);
			break;
		case 2:
			glUniform2fv(_location, _arraySize, _values);
			break;
		case 3:
			glUniform3fv(_location, _arraySize, _values);
			break;
		case 4:
			glUniform4fv(_location, _arraySize, _values);
			break;
		}

		_modified = false;
	}
	FloatUniform::~FloatUniform()
	{
		delete[] _values;
	}
	void FloatUniform::setf1(float f0, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = f0;
	}
	void FloatUniform::setf2(float f0, float f1, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = f0;
		_values[index * _size + 1] = f1;
	}
	void FloatUniform::setf3(float f0, float f1, float f2, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = f0;
		_values[index * _size + 1] = f1;
		_values[index * _size + 2] = f2;
	}
	void FloatUniform::setf4(float f0, float f1, float f2, float f3, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = f0;
		_values[index * _size + 1] = f1;
		_values[index * _size + 2] = f2;
		_values[index * _size + 3] = f3;
	}

	IntUniform::IntUniform(ShaderProgram* program, GLint location, std::string name, GLsizei size, GLsizei arraySize)
		: VecUniform(program, location, name, size, arraySize), _values(new int[size * arraySize])
	{
		for(int i = 0; i < size * arraySize; i++)
		{
			_values[i] = 0;
		}
	}
	void IntUniform::_upload()
	{
		if(!_modified) return;
		switch(_size)
		{
		case 1:
			glUniform1iv(_location, _arraySize, _values);
			break;
		case 2:
			glUniform2iv(_location, _arraySize, _values);
			break;
		case 3:
			glUniform3iv(_location, _arraySize, _values);
			break;
		case 4:
			glUniform4iv(_location, _arraySize, _values);
			break;
		}

		_modified = false;
	}
	IntUniform::~IntUniform()
	{
		delete[] _values;
	}
	void IntUniform::seti1(int i0, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = i0;
	}
	void IntUniform::seti2(int i0, int i1, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = i0;
		_values[index * _size + 1] = i1;
	}
	void IntUniform::seti3(int i0, int i1, int i2, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = i0;
		_values[index * _size + 1] = i1;
		_values[index * _size + 2] = i2;
	}
	void IntUniform::seti4(int i0, int i1, int i2, int i3, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = i0;
		_values[index * _size + 1] = i1;
		_values[index * _size + 2] = i2;
		_values[index * _size + 3] = i3;
	}

	UintUniform::UintUniform(ShaderProgram* program, GLint location, std::string name, GLsizei size, GLsizei arraySize)
		: VecUniform(program, location, name, size, arraySize), _values(new unsigned int[size * arraySize])
	{
		for(int i = 0; i < size * arraySize; i++)
		{
			_values[i] = 0;
		}
	}
	void UintUniform::_upload()
	{
		if(!_modified) return;
		switch(_size)
		{
		case 1:
			glUniform1uiv(_location, _arraySize, _values);
			break;
		case 2:
			glUniform2uiv(_location, _arraySize, _values);
			break;
		case 3:
			glUniform3uiv(_location, _arraySize, _values);
			break;
		case 4:
			glUniform4uiv(_location, _arraySize, _values);
			break;
		}

		_modified = false;
	}
	UintUniform::~UintUniform()
	{
		delete[] _values;
	}
	void UintUniform::setui1(unsigned int ui0, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = ui0;
	}
	void UintUniform::setui2(unsigned int ui0, unsigned int ui1, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = ui0;
		_values[index * _size + 1] = ui1;
	}
	void UintUniform::setui3(unsigned int ui0, unsigned int ui1, unsigned int ui2, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = ui0;
		_values[index * _size + 1] = ui1;
		_values[index * _size + 2] = ui2;
	}
	void UintUniform::setui4(unsigned int ui0, unsigned int ui1, unsigned int ui2, unsigned int ui3, unsigned int index)
	{
		_modified = true;
		_values[index * _size + 0] = ui0;
		_values[index * _size + 1] = ui1;
		_values[index * _size + 2] = ui2;
		_values[index * _size + 3] = ui3;
	}

	MatrixUniform::MatrixUniform(ShaderProgram* program, GLint location, std::string name, GLsizei arraySize)
		: Uniform(program, location, name, arraySize), _values(new Matrix[arraySize])
	{
		for(int i = 0; i < arraySize; i++)
		{
			_values[i] = Matrix::identity();
		}
	}
	void MatrixUniform::_upload()
	{
		if(!_modified) return;
		glUniformMatrix4fv(_location, _arraySize, GL_FALSE, (const GLfloat*)&_values[0]);
		_modified = false;
	}
	MatrixUniform::~MatrixUniform()
	{
		delete[] _values;
	}
	void MatrixUniform::setm(Matrix matrix, unsigned int index)
	{
		_modified = true;
		_values[index] = matrix;
	}

	Matrix3Uniform::Matrix3Uniform(ShaderProgram* program, GLint location, std::string name, GLsizei arraySize)
		: Uniform(program, location, name, arraySize), _values(new Matrix3[arraySize])
	{
		for(int i = 0; i < arraySize; i++)
		{
			_values[i] = Matrix3::identity();
		}
	}
	void Matrix3Uniform::_upload()
	{
		if(!_modified) return;
		glUniformMatrix3fv(_location, _arraySize, GL_FALSE, (const GLfloat*)&_values[0]);
		_modified = false;
	}
	Matrix3Uniform::~Matrix3Uniform()
	{
		delete[] _values;
	}
	void Matrix3Uniform::setm3(Matrix3 matrix, unsigned int index)
	{
		_modified = true;
		_values[index] = matrix;
	}

	ShaderProgram* ShaderProgram::_currentProgram = nullptr;
	void ShaderProgram::_attachShader(Shader* shader)
	{
		shader->_compile();
		glAttachShader(_shaderProgram, shader->_shader);
	}
	void ShaderProgram::_link()
	{
		if(_linked) return;

		_shaderProgram = glCreateProgram();

		if(_vertexShader) _attachShader(_vertexShader);
		if(_geometryShader) _attachShader(_geometryShader);
		if(_fragmentShader) _attachShader(_fragmentShader);

		glLinkProgram(_shaderProgram);
		GLint linkStatus;
		glGetObjectParameterivARB(_shaderProgram, GL_OBJECT_LINK_STATUS_ARB, &linkStatus);

		if(linkStatus == GL_FALSE)
		{
			utility::MetaException exception("GL ERROR: Failed to link shader program");
			if(_vertexShader) exception.addMeta("vertex shader", _vertexShader->name);
			if(_geometryShader) exception.addMeta("geometry shader", _geometryShader->name);
			if(_fragmentShader) exception.addMeta("fragment shader", _fragmentShader->name);

			GLint len;
			glGetProgramiv(_shaderProgram, GL_INFO_LOG_LENGTH, &len);

			if(len > 0)
			{
				GLchar* str = new GLchar[len];
				glGetProgramInfoLog(_shaderProgram, len, nullptr, str);
				std::string logString = std::string(str);
				delete[] str;

				exception.addMeta("log", logString);
			}
			else
			{
				exception.addMeta("log", "No log returned");
			}

			throw exception;
		}

		GLint numUniforms;
		glGetProgramiv(_shaderProgram, GL_ACTIVE_UNIFORMS, &numUniforms);

		GLchar name[256];
		GLsizei actualLength = 0;
		GLint arraySize = 0;
		GLint location = 0;
		GLenum type = 0;

		for(GLint i = 0; i < numUniforms; i++)
		{
			glGetActiveUniform(_shaderProgram, i, 256, &actualLength, &arraySize, &type, name);
			location = glGetUniformLocation(_shaderProgram, name);

			_uniforms[name] = Uniform::_factory(this, name, location, type, arraySize);
		}

		_linked = true;
	}
	ShaderProgram* ShaderProgram::getCurrent()
	{
		return _currentProgram;
	}
	ShaderProgram::ShaderProgram(std::string name)
		: _shaderProgram(0), _linked(false), _vertexShader(nullptr), _geometryShader(nullptr), _fragmentShader(nullptr), name(name)
	{}
	ShaderProgram::~ShaderProgram()
	{
		if(_linked)
		{
			glUseProgram(0);
			glDeleteProgram(_shaderProgram);
		}
	}
	void ShaderProgram::setVertexShader(VertexShader& vertexShader)
	{
		if(_linked)
		{
			utility::MetaException exception("Cannot set vertex shader, shader program is already linked");
			exception.addMeta("shader program", name);
			exception.addMeta("vertex shader", vertexShader.name);
			throw exception;
		}
		_vertexShader = &vertexShader;
	}
	void ShaderProgram::setGeometryShader(GeometryShader& geometryShader)
	{
		if(_linked)
		{
			utility::MetaException exception("Cannot set geometry shader, shader program is already linked");
			exception.addMeta("shader program", name);
			exception.addMeta("geometry shader", geometryShader.name);
			throw exception;
		}
		_geometryShader = &geometryShader;
	}
	void ShaderProgram::setFragmentShader(FragmentShader& fragmentShader)
	{
		if(_linked)
		{
			utility::MetaException exception("Cannot set fragment shader, shader program is already linked");
			exception.addMeta("shader program", name);
			exception.addMeta("fragment shader", fragmentShader.name);
			throw exception;
		}
		_fragmentShader = &fragmentShader;
	}
	void ShaderProgram::bind()
	{
		_link();

		glUseProgram(_shaderProgram);

		_currentProgram = this;
	}
	std::shared_ptr<Uniform> ShaderProgram::getUniform(std::string uniformName)
	{
		_link();

		auto uniform = _uniforms.find(uniformName);
		if(uniform == _uniforms.end())
		{
			utility::MetaException exception("GL ERROR: Uniform does not exist. It may have been optimized away");
			exception.addMeta("uniform name", uniformName);
			exception.addMeta("shader program", name);
			throw exception;
		}

		return uniform->second;
	}
	std::shared_ptr<Uniform> ShaderProgram::tryGetUniform(std::string uniformName)
	{
		_link();

		auto uniform = _uniforms.find(uniformName);
		if(uniform == _uniforms.end())
		{
			return std::shared_ptr<Uniform>();
		}

		return uniform->second;
	}
	void ShaderProgram::uploadUniforms()
	{
		for(auto iter = _uniforms.begin(); iter != _uniforms.end(); ++iter)
		{
			iter->second->_upload();
		}
	}
}
