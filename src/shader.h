#ifndef SHADER_H
#define SHADER_H

#include "matrix.h"
#include <GL/glew.h>
#include <string>
#include <string.h>
#include <memory>
#include <map>
#include <stack>
#include <set>

namespace Graphics
{
	class Uniform;

	template<typename datatype>
	class MatrixStack
	{
	private:
		std::stack<datatype> _computedStack;
		std::set<std::shared_ptr<Uniform>> _matrixUniforms;

		void _updateUniforms();
		void _uploadUniform(std::shared_ptr<Uniform> uniform);

	public:
		bool _modified;

		MatrixStack();
		void push(datatype matrix = datatype::identity());
		void mult(datatype matrix);
		void identity();
		void pop();
		void clear();
		const datatype get();
		void bindMatrixUniform(std::shared_ptr<Uniform> uniform);
		void unbindMatrixUniform(std::shared_ptr<Uniform> uniform);
		void unbindAllMatrixUniforms();
	};

	class Shader
	{
	protected:
		GLuint _shader;
		GLenum _type;
		std::string _content;
		bool _compiled;

		Shader(std::string name, std::string content, std::map<std::string, std::string> params, GLenum type);
		void _compile();

		friend class ShaderProgram;
	public:
		std::string name;
		~Shader();
	};

	class VertexShader : public Shader
	{
	public:
		VertexShader(std::string name, std::string content, std::map<std::string, std::string> params = std::map<std::string, std::string>());
	};

	class GeometryShader : public Shader
	{
	public:
		GeometryShader(std::string name, std::string content, std::map<std::string, std::string> params = std::map<std::string, std::string>());
	};

	class FragmentShader : public Shader
	{
	public:
		FragmentShader(std::string name, std::string content, std::map<std::string, std::string> params = std::map<std::string, std::string>());
	};

	class ShaderProgram;

	class Uniform
	{
	private:
		static std::shared_ptr<Uniform> _factory(ShaderProgram* program, std::string name, GLint location, GLint type, GLint arraySize);
	protected:
		const GLint _location;
		const GLint _arraySize;
		bool _modified;

		Uniform(ShaderProgram* program, GLint location, std::string name, GLsizei arraySize);
		virtual void _upload();
		void _defaultFailure();
	public:
		const ShaderProgram* program;
		const std::string name;

		virtual ~Uniform();
		virtual void setf1(GLfloat f0, unsigned int index = 0) { _defaultFailure(); };
		virtual void setf2(GLfloat f0, GLfloat f1, unsigned int index = 0) { _defaultFailure(); };
		virtual void setf3(GLfloat f0, GLfloat f1, GLfloat f2, unsigned int index = 0) { _defaultFailure(); };
		virtual void setf4(GLfloat f0, GLfloat f1, GLfloat f2, GLfloat f3, unsigned int index = 0) { _defaultFailure(); };
		virtual void seti1(GLint i0, unsigned int index = 0) { _defaultFailure(); };
		virtual void seti2(GLint i0, GLint i1, unsigned int index = 0) { _defaultFailure(); };
		virtual void seti3(GLint i0, GLint i1, GLint i2, unsigned int index = 0) { _defaultFailure(); };
		virtual void seti2(GLint i0, GLint i1, GLint i2, GLint i3, unsigned int index = 0) { _defaultFailure(); };
		virtual void setui1(GLuint ui0, unsigned int index = 0) { _defaultFailure(); };
		virtual void setui2(GLuint ui0, GLuint ui1, unsigned int index = 0) { _defaultFailure(); };
		virtual void setui3(GLuint ui0, GLuint ui1, GLuint ui2, unsigned int index = 0) { _defaultFailure(); };
		virtual void setui2(GLuint ui0, GLuint ui1, GLuint ui2, GLuint ui3, unsigned int index = 0) { _defaultFailure(); };
		virtual void setm(Matrix matrix, unsigned int index = 0) { _defaultFailure(); };
		virtual void setm3(Matrix3 matrix, unsigned int index = 0) { _defaultFailure(); };

		friend class ShaderProgram;
	};

	class VecUniform : public Uniform
	{
	protected:
		const GLsizei _size;

		VecUniform(ShaderProgram* program, GLint location, std::string name, GLsizei size, GLsizei arraySize);
	};

	class FloatUniform : public VecUniform
	{
	private:
		GLfloat* _values;

		FloatUniform(ShaderProgram* program, GLint location, std::string name, GLsizei size, GLsizei arraySize);
		virtual void _upload();
	public:
		virtual ~FloatUniform();
		virtual void setf1(GLfloat f0, unsigned int index = 0);
		virtual void setf2(GLfloat f0, GLfloat f1, unsigned int index = 0);
		virtual void setf3(GLfloat f0, GLfloat f1, GLfloat f2, unsigned int index = 0);
		virtual void setf4(GLfloat f0, GLfloat f1, GLfloat f2, GLfloat f3, unsigned int index = 0);

		friend class Uniform;
	};

	class IntUniform : public VecUniform
	{
	private:
		GLint* _values;

		IntUniform(ShaderProgram* program, GLint location, std::string name, GLsizei size, GLsizei arraySize);
		virtual void _upload();
	public:
		virtual ~IntUniform();
		virtual void seti1(GLint i0, unsigned int index = 0);
		virtual void seti2(GLint i0, GLint i1, unsigned int index = 0);
		virtual void seti3(GLint i0, GLint i1, GLint i2, unsigned int index = 0);
		virtual void seti4(GLint i0, GLint i1, GLint i2, GLint i3, unsigned int index = 0);

		friend class Uniform;
	};

	class UintUniform : public VecUniform
	{
	private:
		GLuint* _values;

		UintUniform(ShaderProgram* program, GLint location, std::string name, GLsizei size, GLsizei arraySize);
		virtual void _upload();
	public:
		virtual ~UintUniform();
		virtual void setui1(GLuint i0, unsigned int index = 0);
		virtual void setui2(GLuint i0, GLuint i1, unsigned int index = 0);
		virtual void setui3(GLuint i0, GLuint i1, GLuint i2, unsigned int index = 0);
		virtual void setui4(GLuint i0, GLuint i1, GLuint i2, GLuint i3, unsigned int index = 0);

		friend class Uniform;
	};

	class MatrixUniform : public Uniform
	{
	private:
		Matrix* _values;

		MatrixUniform(ShaderProgram* program, GLint location, std::string name, GLsizei arraySize);
		virtual void _upload();
	public:
		virtual ~MatrixUniform();
		virtual void setm(Matrix i0, unsigned int index = 0);

		friend class Uniform;
	};

	class Matrix3Uniform : public Uniform
	{
	private:
		Matrix3* _values;

		Matrix3Uniform(ShaderProgram* program, GLint location, std::string name, GLsizei arraySize);
		virtual void _upload();
	public:
		virtual ~Matrix3Uniform();
		virtual void setm3(Matrix3 i0, unsigned int index = 0);

		friend class Uniform;
	};

	class ShaderProgram
	{
	private:
		static ShaderProgram* _currentProgram;

		GLuint _shaderProgram;
		bool _linked;
		VertexShader* _vertexShader;
		GeometryShader* _geometryShader;
		FragmentShader* _fragmentShader;
		std::map<std::string, std::shared_ptr<Uniform>> _uniforms;
		void _attachShader(Shader* shader);
		void _link();
	public:
		const std::string name;

		static ShaderProgram* getCurrent();

		ShaderProgram(std::string name);
		~ShaderProgram();
		void setVertexShader(VertexShader& vertexShader);
		void setGeometryShader(GeometryShader& geometryShader);
		void setFragmentShader(FragmentShader& fragmentShader);
		void bind();
		std::shared_ptr<Uniform> getUniform(std::string uniformName);
		std::shared_ptr<Uniform> tryGetUniform(std::string uniformName);
		void uploadUniforms();
	};
}

#endif
