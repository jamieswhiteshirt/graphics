#include "skeletalmesh.h"

namespace Graphics
{
	SkeletalMesh::Node::Node(unsigned char index, unsigned char parentIndex, utility::Vector3f originalPosition, utility::Quaternion originalOrientation, float originalLength)
		: index(index), parentIndex(parentIndex), originalPosition(originalPosition), originalOrientation(originalOrientation), originalLength(originalLength), scale(1.0f)
	{}
	Matrix SkeletalMesh::Node::getMatrix()
	{
		return Matrix::translate(originalPosition) * Matrix::rotate(rotation) * Matrix::translate(-originalPosition + rotation.conjugate() * offset);
	}
	Matrix3 SkeletalMesh::Node::getNormalMatrix()
	{
		return Matrix3::rotate(rotation);
	}
	void SkeletalMesh::Node::computeRecursively(std::map<unsigned char, std::shared_ptr<Node>>& nodes, utility::Vector3f v, utility::Quaternion q)
	{
		q *= orientation;
		rotation = q / originalOrientation;

		offset = v - originalPosition;

		if(children.size() > 0)
		{
			v += q * (utility::Vector3f(originalLength, 0.0f, 0.0f) * scale);
			for(unsigned int i = 0; i < children.size(); i++)
			{
				nodes[children[i]]->computeRecursively(nodes, v, q);
			}
		}
	}
	utility::Vector3f SkeletalMesh::Node::originalEnd()
	{
		return originalPosition + originalOrientation * utility::Vector3f(originalLength, 0.0f, 0.0f);
	}

	SkeletalMesh::SkeletalMesh()
	{}
	void SkeletalMesh::bindMatrixUniform(std::shared_ptr<Uniform> matrixUniform)
	{
		_matrixUniforms.push_back(matrixUniform);
	}
	void SkeletalMesh::bindNormalMatrixUniform(std::shared_ptr<Uniform> normalMatrixUniform)
	{
		_normalMatrixUniforms.push_back(normalMatrixUniform);
	}
	void SkeletalMesh::addRootNode(unsigned char index, utility::Vector3f offset, utility::Quaternion orientation, float length)
	{
		_nodes[index] = std::shared_ptr<Node>(new Node(index, index, offset, orientation, length));
	}
	void SkeletalMesh::addNode(unsigned char index, unsigned char parentIndex, utility::Quaternion orientation, float length)
	{
		std::shared_ptr<Node> parent = _nodes[parentIndex];

		_nodes[index] = std::shared_ptr<Node>(new Node(index, parentIndex, parent->originalEnd(), parent->originalOrientation * orientation, length));
		parent->children.push_back(index);
	}
	utility::Quaternion& SkeletalMesh::orientation(unsigned char index)
	{
		return _nodes[index]->orientation;
	}
	float& SkeletalMesh::scale(unsigned char index)
	{
		return _nodes[index]->scale;
	}
	void SkeletalMesh::compute()
	{
		for(auto iter = _nodes.begin(); iter != _nodes.end(); ++iter)
		{
			if(iter->second->index == iter->second->parentIndex)
			{
				iter->second->computeRecursively(_nodes, iter->second->originalPosition, iter->second->originalOrientation);
			}
		}

		for(auto iter = _nodes.begin(); iter != _nodes.end(); ++iter)
		{
			Matrix matrix = iter->second->getMatrix();

			for(auto iter1 = _matrixUniforms.begin(); iter1 != _matrixUniforms.end(); ++iter1)
			{
				(*iter1)->setm(matrix, iter->first);
			}

			Matrix3 normalMatrix = iter->second->getNormalMatrix();

			for(auto iter1 = _normalMatrixUniforms.begin(); iter1 != _normalMatrixUniforms.end(); ++iter1)
			{
				(*iter1)->setm3(normalMatrix, iter->first);
			}
		}
	}
}