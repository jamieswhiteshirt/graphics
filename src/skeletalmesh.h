#ifndef SKELETALMESH_H
#define SKELETALMESH_H

#include "vertexstream.h"
#include "shader.h"
#include <vector.h>

namespace Graphics
{
	class SkeletalMesh
	{
	private:
		class Node
		{
		public:
			const unsigned char index;
			const unsigned char parentIndex;
			const utility::Vector3f originalPosition;
			const utility::Quaternion originalOrientation;
			const float originalLength;

			std::vector<unsigned char> children;

			utility::Quaternion orientation;
			utility::Quaternion rotation;
			utility::Vector3f offset;
			float scale;

			Node(unsigned char index, unsigned char parentIndex, utility::Vector3f originalPosition, utility::Quaternion originalOrientation, float originalLength);
			Matrix getMatrix();
			Matrix3 getNormalMatrix();
			void computeRecursively(std::map<unsigned char, std::shared_ptr<Node>>& nodes, utility::Vector3f v, utility::Quaternion q);
			utility::Vector3f originalEnd();
		};

		std::vector<std::shared_ptr<Uniform>> _matrixUniforms;
		std::vector<std::shared_ptr<Uniform>> _normalMatrixUniforms;
		std::map<unsigned char, std::shared_ptr<Node>> _nodes;

	public:
		SkeletalMesh();
		
		void bindMatrixUniform(std::shared_ptr<Uniform> matrixUniform);
		void bindNormalMatrixUniform(std::shared_ptr<Uniform> normalMatrixUniform);
		void addRootNode(unsigned char index, utility::Vector3f offset, utility::Quaternion orientation, float length);
		void addNode(unsigned char index, unsigned char parentIndex, utility::Quaternion orientation, float length);
		utility::Quaternion& orientation(unsigned char index);
		float& scale(unsigned char index);

		void compute();
	};
}

#endif