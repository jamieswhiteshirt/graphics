#include "texture.h"
#include "graphicserror.h"
#include <algorithm>
#include <string.h>

namespace Graphics
{
	bool TexImage::_upload(GLenum textureTarget, GLenum target)
	{
		if(_uploaded) return false;

		if(_texture == 0) glGenTextures(1, &_texture);
		glBindTexture(textureTarget, _texture);
		glTexImage2D(target, 0, _internalFormat, _width, _height, 0, _format, _type, _dataPtr());

		_uploaded = true;
		return true;
	}
	const GLvoid* TexImage::_dataPtr()
	{
		return nullptr;
	}
	TexImage::TexImage(GLint internalFormat, GLenum format, GLenum type, int width, int height)
		: _internalFormat(internalFormat), _format(format), _type(type), _width(width), _height(height), _texture(0), _uploaded(false)
	{}

	AbstractTexture2D::AbstractTexture2D(GLint internalFormat, GLenum format, GLenum type, GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, int width, int height)
		: TexImage(internalFormat, format, type, width, height), _magFilter(magFilter), _minFilter(minFilter), _wrap(wrap), _mipmapped(mipmapped)
	{}
	bool AbstractTexture2D::_upload(GLenum textureTarget, GLenum target)
	{
		if(TexImage::_upload(textureTarget, target))
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, _magFilter);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, _minFilter);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, _wrap);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, _wrap);

			if(_mipmapped) glGenerateMipmap(GL_TEXTURE_2D);

			return true;
		}
		else
		{
			return false;
		}
	}
	AbstractTexture2D::~AbstractTexture2D()
	{
		if(_uploaded) glDeleteTextures(1, &_texture);
	}
	void AbstractTexture2D::upload()
	{
		_upload(GL_TEXTURE_2D, GL_TEXTURE_2D);
	}
	void AbstractTexture2D::bind(int unit)
	{
		upload();

		glActiveTexture(GL_TEXTURE0 + unit);
		glBindTexture(GL_TEXTURE_2D, _texture);
	}
	void AbstractTexture2D::resize(int width, int height)
	{
		_width = width;
		_height = height;
		if (_uploaded) glTexImage2D(GL_TEXTURE_2D, 0, _internalFormat, _width, _height, 0, _format, _type, 0);
	}
	void AbstractTexture2D::unbind(int unit)
	{
		glActiveTexture(GL_TEXTURE0 + unit);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	const GLvoid* Texture2D::_dataPtr()
	{
		return _data;
	}
	Texture2D::Texture2D(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped)
		: AbstractTexture2D(GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, magFilter, minFilter, wrap, mipmapped), _data(nullptr)
	{}
	void Texture2D::_editSubRegion(const unsigned char* data, int dst_x, int dst_y, int src_width, int src_height, int src_x, int src_y, int width, int height)
	{
		int dstWidth = this->_width * 4;
		int subWidth = width * 4;
		int srcWidth = src_width * 4;
		unsigned char* subImg = _uploaded ? new unsigned char[width * height * 4] : nullptr;

		for (int y = 0; y < height; y++)
		{
			memcpy(&this->_data[dstWidth * (this->_height - dst_y - y - 1) + dst_x * 4],&data[srcWidth * (src_y + y) + src_x * 4], subWidth);
			if (subImg) memcpy(&subImg[subWidth * y], &data[srcWidth * (height - src_y - y - 1) + src_x * 4], subWidth);
		}

		if (_uploaded)
		{
			glBindTexture(GL_TEXTURE_2D, _texture);
			glTexSubImage2D(GL_TEXTURE_2D, 0, dst_x, this->_height - dst_y - height, width, height, GL_RGBA, GL_UNSIGNED_BYTE, subImg);
		}

		if (subImg) delete[] subImg;
	}
	Texture2D::Texture2D(Bitmap& bitmap, GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped)
		: AbstractTexture2D(GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, magFilter, minFilter, wrap, mipmapped, bitmap._width, bitmap._height), _data(new unsigned char[bitmap._width * bitmap._height * 4])
	{
		for(unsigned int i = 0; i < bitmap._height; i++)
		{
			memcpy(_data + i * bitmap._width * 4, bitmap._data + (bitmap._height - 1 - i) * bitmap._width * 4, bitmap._width * 4);
		}
	}
	Texture2D::Texture2D(int width, int height, GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped)
		: AbstractTexture2D(GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, magFilter, minFilter, wrap, mipmapped, width, height), _data(nullptr)
	{}
	Texture2D::~Texture2D()
	{
		if(_data)
		{
			delete[] _data;
		}
	}
	void Texture2D::subImage(const Bitmap& bitmap, int dst_x, int dst_y, int src_x, int src_y, int width, int height)
	{
		if (width == 0) width = bitmap._width;
		if (height == 0) height = bitmap._height;

		this->_editSubRegion(bitmap._data, dst_x, dst_y, bitmap._width, bitmap._height, src_x, src_y, width, height);
	}
	void Texture2D::resize(int width, int height)
	{
		unsigned char* tmpData = _data;
		_data = new unsigned char[width * height * 4];
		memset(&_data[0], 0, width * height * 4);
		if (tmpData != nullptr)
		{
			//_editSubRegion(tmpData, 0, 0, tmpWidth, tmpHeight, 0, 0, min(width, tmpWidth), min(height, tmpHeight));
			for (unsigned int i = 0; i < _height; i++)
			{
				// TODO: Make sure it does not cock up if the picture is minified
				memcpy(&_data[(height - _height + i) * width * 4], &tmpData[i * _width * 4], (_width < width ? _width : width) * 4);
			}
			delete[] tmpData;
		}

		_uploaded = false;
		AbstractTexture2D::resize(width, height);
	}

	DepthTexture2D::DepthTexture2D(int width, int height)
		: AbstractTexture2D(GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT, GL_FLOAT, GL_NEAREST, GL_NEAREST, GL_CLAMP_TO_EDGE, false, width, height)
	{}
}
