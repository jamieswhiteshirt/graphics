#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>
#include "bitmap.h"

namespace Graphics
{
	class TexImage
	{
	protected:
		const GLint _internalFormat;
		const GLenum _format;
		const GLenum _type;
		int _width, _height;

		GLuint _texture;
		bool _uploaded;

		virtual bool _upload(GLenum textureTarget, GLenum target);
		virtual const GLvoid* _dataPtr();

	public:
		TexImage(GLint internalFormat, GLenum format, GLenum type, int width, int height);
	};

	class AbstractTexture2D : public TexImage
	{
	private:
		GLenum _magFilter;
		GLenum _minFilter;
		GLenum _wrap;

	protected:
		bool _mipmapped;

		AbstractTexture2D(GLint internalFormat, GLenum format, GLenum type, GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped = false, int width = 0, int height = 0);
		virtual bool _upload(GLenum textureTarget, GLenum target);

	public:
		virtual ~AbstractTexture2D();
		int width() { return _width; }
		int height() { return _height; }

		void upload();
		virtual void bind(int unit = 0);
		virtual void resize(int width, int height);

		static void unbind(int unit = 0);

		friend class FrameBuffer;
	};

	class Texture2D : public AbstractTexture2D
	{
	protected:
		unsigned char* _data;

		virtual const GLvoid* _dataPtr();
		Texture2D(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped);
		virtual void _editSubRegion(const unsigned char* data, int dst_x, int dst_y, int src_width, int src_height, int src_x, int src_y, int width, int height);
	public:
		Texture2D(Bitmap& bitmap, GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped);
		Texture2D(int width, int height, GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped);
		virtual ~Texture2D();

		void subImage(const Bitmap& bitmap, int dst_x, int dst_y, int src_x = 0, int src_y = 0, int width = 0, int height = 0);
		virtual void resize(int width, int height);
	};

	class DepthTexture2D : public AbstractTexture2D
	{
	public:
		DepthTexture2D(int width, int height);
	};
}

#endif
