#include "tiledtexture.h"
#include <algorithm>
#include <debug.h>

unsigned int closest2(unsigned int i)
{
	unsigned int j = 1;
	while((j <<= 1) < i);
	return j;
}

namespace Graphics
{
	template<typename Key>
	TiledTexture<Key>::Icon::Icon(TiledTexture* texture, Bitmap& bitmap, int x, int y, bool rotated)
		: texture(texture), bitmap(bitmap), x(x), y(y), rotated(rotated)
	{
		if (rotated)
		{
			this->bitmap.rotate();
		}
	}
	template<typename Key>
	UV TiledTexture<Key>::Icon::getUV(float ui, float vi)
	{
		return UV((!rotated ? (x + ui * bitmap._width) : (x + vi * bitmap._width)) / (texture->_absoluteUV ? 1 : texture->_width),
			(!rotated ? (texture->_height - (y + (1.0f - vi) * bitmap._height)) : (texture->_height - (y + ui * bitmap._height))) / (texture->_absoluteUV ? 1 : texture->_height));
	}
	template<typename Key>
	UV TiledTexture<Key>::Icon::getUV(UV uv)
	{
		return getUV(uv._v, uv._u);
	}
	template<typename Key>
	int TiledTexture<Key>::Icon::width() const
	{
		return bitmap._width;
	}
	template<typename Key>
	int TiledTexture<Key>::Icon::height() const
	{
		return bitmap._height;
	}
	template<typename Key>
	int TiledTexture<Key>::Icon::area() const
	{
		return bitmap._width * bitmap._height;
	}
	template<typename Key>
	const unsigned char* TiledTexture<Key>::Icon::data()
	{
		return bitmap._data;
	}

	template<typename Key>
	bool compareIcon(std::shared_ptr<typename TiledTexture<Key>::Icon> first, std::shared_ptr<typename TiledTexture<Key>::Icon> second)
	{
		return closest2(first->width()) * closest2(first->height()) > closest2(second->width()) * closest2(second->height());
	}
	inline unsigned int coord_decode(unsigned int coord)
	{
		unsigned int res = 0;
		for(unsigned int i = 0; i < 16; i++)
		{
			res |= (coord & 1) << i;
			coord >>= 2;
		}

		return res;
	}

	template<typename Key>
	TiledTexture<Key>::TiledTexture(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV)
		: Texture2D(magFilter, minFilter, wrap, mipmapped), _absoluteUV(absoluteUV)
	{}
	template<typename Key>
	TiledTexture<Key>::~TiledTexture()
	{}
	template<typename Key>
	std::shared_ptr<typename TiledTexture<Key>::Icon> TiledTexture<Key>::getIcon(Bitmap& bitmap, Key identifier)
	{
		auto iconIter = _icons.find(identifier);
		if(iconIter == _icons.end())
		{
			int x = 0, y = 0;
			bool rotated = false;
			_iconAdded(bitmap._width, bitmap._height, x, y, rotated);
			std::shared_ptr<Icon> icon(new Icon(this, bitmap, x, y, rotated));
			_icons[identifier] = icon;
			if (this->_dynamic()) this->subImage(icon->bitmap, x, y);
			return icon;
		}
		else
		{
			return iconIter->second;
		}
	}
	template<typename Key>
	std::shared_ptr<typename TiledTexture<Key>::Icon> TiledTexture<Key>::getIcon(Key identifier)
	{
		auto iconIter = _icons.find(identifier);
		if(iconIter == _icons.end())
		{
			return nullptr;
		}
		else
		{
			return iconIter->second;
		}
	}

	template<typename Key>
	StaticTiledTexture<Key>::StaticTiledTexture(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV)
		: TiledTexture<Key>(magFilter, minFilter, wrap, mipmapped, absoluteUV), _compiled(false)
	{}

	template<typename Key>
	StaticTiledTexture<Key>::~StaticTiledTexture()
	{}

	template<typename Key>
	void StaticTiledTexture<Key>::compile()
	{
		if(_compiled) return;

		tile();

		for(auto iter = this->_icons.begin(); iter != this->_icons.end(); ++iter)
		{
			std::shared_ptr<typename TiledTexture<Key>::Icon> icon = iter->second;

			if(this->_width < icon->x + icon->width()) this->_width = icon->x + icon->width();
			if(this->_height < icon->y + icon->height()) this->_height = icon->y + icon->height();
		}

		this->_data = new unsigned char[this->_width * this->_height * 4];
		memset(this->_data, 0, this->_width * this->_height * 4);

		for(auto iter = this->_icons.begin(); iter != this->_icons.end(); ++iter)
		{
			std::shared_ptr<typename TiledTexture<Key>::Icon> icon = iter->second;

			this->subImage(icon->bitmap, icon->x, icon->y);
		}

		_compiled = true;
	}
	template<typename Key>
	void StaticTiledTexture<Key>::bind(int unit)
	{
		compile();

		Texture2D::bind(unit);
	}

	template<typename Key>
	void CheapTiledTexture<Key>::tile()
	{
		std::vector<std::shared_ptr<typename TiledTexture<Key>::Icon>> sortedIcons;

		for(auto iter = this->_icons.begin(); iter != this->_icons.end(); ++iter)
		{
			sortedIcons.push_back(iter->second);
		}

		std::sort(sortedIcons.begin(), sortedIcons.end(), compareIcon<Key>);

		for(unsigned int i = 0, index = 0; i < sortedIcons.size(); i++)
		{
			std::shared_ptr<typename TiledTexture<Key>::Icon> icon = sortedIcons[i];

			icon->x = coord_decode(index);
			icon->y = coord_decode(index >> 1);

			unsigned int technicalWidth = closest2(icon->width());
			unsigned int technicalHeight = closest2(icon->height());
			index += technicalWidth > technicalHeight ? (technicalWidth * technicalWidth) : (technicalHeight * technicalHeight);
		}
	}
	template<typename Key>
	CheapTiledTexture<Key>::CheapTiledTexture(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV)
		: StaticTiledTexture<Key>(magFilter, minFilter, wrap, mipmapped, absoluteUV)
	{}
	template<typename Key>
	CheapTiledTexture<Key>::~CheapTiledTexture()
	{}

	template<typename Key>
	DynamicTiledTexture<Key>::DynamicTiledTexture(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV)
		: TiledTexture<Key>(magFilter, minFilter, wrap, mipmapped, absoluteUV)
	{}
	template<typename Key>
	DynamicTiledTexture<Key>::~DynamicTiledTexture()
	{}

	template<typename Key>
	void DynamicTiledTexture<Key>::bind(int unit)
	{
		Texture2D::bind(unit);
	}

	template<typename Key>
	MaxRectsTiledTexture<Key>::MaxRectsTiledTexture(int initialSize,GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV)
		: DynamicTiledTexture<Key>(magFilter, minFilter, wrap, mipmapped, absoluteUV)
	{
		this->resize(initialSize, initialSize);
		surface.push_back(ray(0, 0, initialSize));
	}

	template<typename Key>
	MaxRectsTiledTexture<Key>::~MaxRectsTiledTexture()
	{}

	template<typename Key>
	void MaxRectsTiledTexture<Key>::_iconAdded(unsigned int width, unsigned int height, int& out_x, int& out_y, bool& out_rotation)
	{
		width += 2;
		height += 2;
		//Max Rectangles Bottom-Left
		// Select the best placement
		int bestY = 0x7fffffff;
		typename std::vector<ray>::iterator bestIt;
		bool bestRot = false;

		for (typename std::vector<ray>::iterator it = surface.begin(); it != surface.end(); it++)
		{
			int placeY = it->x + width <= this->width() ? this->_evalRay(it, width) : 0x7fffffff;
			int tmp = it->x + height <= this->width() ? this->_evalRay(it, height) : 0x7fffffff;
			bool rotated = (tmp + width) < (placeY + height);
			if (rotated) placeY = tmp;

			// Use this position if it is the lowest
			if (placeY + (rotated ? width : height) < bestY + (bestRot ? width : height))
			{
				bestY = placeY;
				bestIt = it;
				bestRot = rotated;
			}
		}

		// Make new array
		std::vector<ray> newSurface;
		int w = bestRot ? height : width;
		int h = bestRot ? width : height;
		int x0 = bestIt->x;
		int lastY = -1;

		if(bestY + h > this->height())
		{
			this->resize(this->width(), this->height() + this->width());
			this->resizeSequenceNumber++;
		}

		for (auto it = surface.begin(); it != surface.end(); it++)
		{
			if (it->x >= x0 && it->x < x0 + w)
			{
				if (lastY != bestY + h)
				{
					newSurface.push_back(ray(x0, bestY + h, w));
					if (x0 + w < it->x + it->width)
					{
						newSurface.push_back(ray(x0 + w, it->y, it->x + it->width - (x0 + w)));
					}
					lastY = bestY + h;
				}
				else
				{
					auto nit = --newSurface.end();
					if(it->x + it->width < x0 + w)
					{
						nit->width += it->width;
					}
					else
					{
						nit->width += x0 + w - it->x;
						newSurface.push_back(ray(x0 + w, it->y, it->x + it->width - (x0 + w)));
					}
				}
			}
			else
			{
				newSurface.push_back(ray(it->x, it->y, it->width));
				lastY = it->y;
			}
		}
		out_x = x0 + 1;
		out_y = bestY + 1;
		out_rotation = bestRot;
		this->surface = newSurface;
	}

	template<typename Key>
	int MaxRectsTiledTexture<Key>::_evalRay(typename std::vector<ray>::iterator r, int width)
	{
		int maxHeight = 0;
		int x0 = r->x;

		for (;r != surface.end() && r->x < (x0 + width); r++)
		{
			if (r->y > maxHeight) maxHeight = r->y;
		}

		return maxHeight;
	}

	template class TiledTexture<std::string>;
	template class TiledTexture<unsigned int>;
	template class StaticTiledTexture<std::string>;
	template class StaticTiledTexture<unsigned int>;
	template class DynamicTiledTexture<std::string>;
	template class DynamicTiledTexture<unsigned int>;
	template class CheapTiledTexture<std::string>;
	template class CheapTiledTexture<unsigned int>;
	template class MaxRectsTiledTexture<std::string>;
	template class MaxRectsTiledTexture<unsigned int>;
}
