#ifndef TILEDTEXTURE_H
#define TILEDTEXTURE_H

#include <GL/glew.h>
#include "texture.h"
#include "vertex.h"
#include <vector>
#include <map>
#include <memory>
#include <string>
#include <string.h>

namespace Graphics
{
	template<typename Key = std::string>
	class TiledTexture : public Texture2D
	{
	public:
		class Icon
		{
		private:
			Icon(TiledTexture<Key>* texture, Bitmap& bitmap, int x = 0, int y = 0, bool rotated = false);
		public:
			TiledTexture<Key>* texture;
			Bitmap bitmap;
			int x, y;
			bool rotated;
			void* tag;

			UV getUV(float ui, float vi);
			UV getUV(UV uv);
			int width() const;
			int height() const;
			int area() const;
			const unsigned char* data();

			friend class TiledTexture;
		};

	protected:
		std::map<Key, std::shared_ptr<typename TiledTexture<Key>::Icon>> _icons;
		virtual void _iconAdded(unsigned int width, unsigned int height, int& out_x, int& out_y, bool& out_rotation) {}
		virtual bool _dynamic() = 0;
		bool _absoluteUV;

	public:
		TiledTexture(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV = false);
		virtual ~TiledTexture();

		std::shared_ptr<typename TiledTexture<Key>::Icon> getIcon(Bitmap& bitmap, Key identifier);
		std::shared_ptr<typename TiledTexture<Key>::Icon> getIcon(Key identifier);
		virtual void bind(int unit = 0) = 0;
	};

	template<typename Key = std::string>
	class StaticTiledTexture : public TiledTexture<Key>
	{
	private:
		bool _compiled;

	protected:
		virtual void tile() = 0;
		virtual bool _dynamic() { return false; }

	public:
		StaticTiledTexture(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV = false);
		virtual ~StaticTiledTexture();

		virtual void compile();
		virtual void bind(int unit = 0);
	};

	template<typename Key = std::string>
	class CheapTiledTexture : public StaticTiledTexture<Key>
	{
	protected:
		virtual void tile();
	public:
		CheapTiledTexture(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV = false);
		~CheapTiledTexture();
	};

	template<typename Key = std::string>
	class DynamicTiledTexture : public TiledTexture<Key>
	{
	protected:
		virtual void _iconAdded(unsigned int width, unsigned int height, int& out_x, int& out_y, bool& out_rotation) = 0;
		virtual bool _dynamic() { return true; }

	public:
		DynamicTiledTexture(GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV = false);
		~DynamicTiledTexture();

		virtual void bind(int unit = 0);
	};

	template<typename Key = std::string>
	class MaxRectsTiledTexture : public DynamicTiledTexture<Key>
	{
	protected:
		virtual void _iconAdded(unsigned int width, unsigned int height, int& out_x, int& out_y, bool& out_rotation);

	public:
		MaxRectsTiledTexture(int initialSize, GLenum magFilter, GLenum minFilter, GLenum wrap, bool mipmapped, bool absoluteUV = false);
		~MaxRectsTiledTexture();
		unsigned long resizeSequenceNumber;

	private:
		struct ray
		{
			ray(int x, int y, int width)
				: x(x), y(y), width(width)
			{}
			int x, y, width;
		};

		std::vector<ray> surface;

		int _evalRay(typename std::vector<ray>::iterator r, int width);
	};
}

#endif
