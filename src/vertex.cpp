#define _USE_MATH_DEFINES

#include "vertex.h"
#include <math.h>
#include "matrix.h"

namespace Graphics
{
	SkeletalWeight::SkeletalWeight(float weight0, float weight1, unsigned char index0, unsigned char index1)
		: _weight0(weight0), _weight1(weight1), _index0(index0), _index1(index1)
	{}

	UV::UV(float u, float v)
		: _u(u), _v(v)
	{}
	bool UV::operator==(const UV& uv) const
	{
		return _u == uv._u && _v == uv._v;
	}
	bool UV::operator<(const Graphics::UV& uv) const
	{
		if(_u == uv._u)
		{
			return _v < uv._v;
		}
		else
		{
			return _u < uv._u;
		}
	}

	RGBA::RGBA(float r, float g, float b, float a)
		: _r(r), _g(g), _b(b), _a(a)
	{}
	bool RGBA::operator==(const RGBA& rgba) const
	{
		return _r == rgba._r && _g == rgba._g && _b == rgba._b && _a == rgba._a;
	}
	bool RGBA::operator<(const RGBA& rgba) const
	{
		if(_r == rgba._r)
		{
			if(_g == rgba._g)
			{
				if(_b == rgba._b)
				{
					return _a < rgba._a;
				}
				else
				{
					return _b < rgba._b;
				}
			}
			else
			{
				return _g < rgba._g;
			}
		}
		else
		{
			return _r < rgba._r;
		}
	}

	Vertex::Vertex(float x, float y, float z)
		: _x(x), _y(y), _z(z)
	{}
	bool Vertex::operator==(const Vertex& v) const
	{
		return _x == v._x && _y == v._y && _z == v._z;
	}
	bool Vertex::operator<(const Graphics::Vertex& v) const
	{
		if(_x == v._x)
		{
			if(_y == v._y)
			{
				return _z < v._z;
			}
			else
			{
				return _y < v._y;
			}
		}
		else
		{
			return _x < v._x;
		}
	}

	VertexUV::VertexUV(float x, float y, float z, float u, float v)
		: Vertex(x, y, z), _u(u), _v(v)
	{}
	VertexUV::VertexUV(Vertex vertex, float u, float v)
		: Vertex(vertex), _u(u), _v(v)
	{}
	VertexUV::VertexUV(float x, float y, float z, UV uv)
		: Vertex(x, y, z), _u(uv._u), _v(uv._v)
	{}
	VertexUV::VertexUV(Vertex vertex, UV uv)
		: Vertex(vertex), _u(uv._u), _v(uv._v)
	{}
	VertexUV::VertexUV(const Vertex& vertex)
		: Vertex(vertex), _u(0.0f), _v(0.0f)
	{}
	bool VertexUV::operator==(const VertexUV& v) const
	{
		return _x == v._x && _y == v._y && _z == v._z && _u == v._u && _v == v._v;
	}
	bool VertexUV::operator<(const Graphics::VertexUV& v) const
	{
		if(_x == v._x)
		{
			if(_y == v._y)
			{
				if(_z == v._z)
				{
					if(_u == v._u)
					{
						return _v < v._v;
					}
					else
					{
						return _u < v._u;
					}
				}
				else
				{
					return _z < v._z;
				}
			}
			else
			{
				return _y < v._y;
			}
		}
		else
		{
			return _x < v._x;
		}
	}

	VertexRGBA::VertexRGBA(float x, float y, float z, float r, float g, float b, float a)
		: Vertex(x, y, z), _r(r), _g(g), _b(b), _a(a)
	{}
	VertexRGBA::VertexRGBA(Vertex vertex, float r, float g, float b, float a)
		: Vertex(vertex), _r(r), _g(g), _b(b), _a(a)
	{}
	VertexRGBA::VertexRGBA(float x, float y, float z, RGBA rgba)
		: Vertex(x, y, z), _r(rgba._r), _g(rgba._g), _b(rgba._b), _a(rgba._a)
	{}
	VertexRGBA::VertexRGBA(Vertex vertex, RGBA rgba)
		: Vertex(vertex), _r(rgba._r), _g(rgba._g), _b(rgba._b), _a(rgba._a)
	{}
	VertexRGBA::VertexRGBA(const Vertex& vertex)
		: Vertex(vertex), _r(1.0f), _g(1.0f), _b(1.0f), _a(1.0f)
	{}
	bool VertexRGBA::operator==(const VertexRGBA& v) const
	{
		return _x == v._x && _y == v._y && _z == v._z && _r == v._r && _g == v._g && _b == v._b && _a == v._a;
	}
	bool VertexRGBA::operator<(const Graphics::VertexRGBA& v) const
	{
		if(_x == v._x)
		{
			if(_y == v._y)
			{
				if(_z == v._z)
				{
					if(_r == v._r)
					{
						if(_g == v._g)
						{
							if(_b == v._b)
							{
								return _a < v._a;
							}
							else
							{
								return _b < v._b;
							}
						}
						else
						{
							return _g < v._g;
						}
					}
					else
					{
						return _r < v._r;
					}
				}
				else
				{
					return _z < v._z;
				}
			}
			else
			{
				return _y < v._y;
			}
		}
		else
		{
			return _x < v._x;
		}
	}

	VertexUVRGBA::VertexUVRGBA(float x, float y, float z, float u, float v, float r, float g, float b, float a)
		: Vertex(x, y, z), _u(u), _v(v), _r(r), _g(g), _b(b), _a(a)
	{}
	VertexUVRGBA::VertexUVRGBA(Vertex vertex, float u, float v, float r, float g, float b, float a)
		: Vertex(vertex), _u(u), _v(v), _r(r), _g(g), _b(b), _a(a)
	{}
	VertexUVRGBA::VertexUVRGBA(float x, float y, float z, UV uv, float r, float g, float b, float a)
		: Vertex(x, y, z), _u(uv._u), _v(uv._v), _r(r), _g(g), _b(b), _a(a)
	{}
	VertexUVRGBA::VertexUVRGBA(Vertex vertex, UV uv, float r, float g, float b, float a)
		: Vertex(vertex), _u(uv._u), _v(uv._v), _r(r), _g(g), _b(b), _a(a)
	{}
	VertexUVRGBA::VertexUVRGBA(VertexUV vertex, float r, float g, float b, float a)
		: Vertex(vertex), _u(vertex._u), _v(vertex._v), _r(r), _g(g), _b(b), _a(a)
	{}
	VertexUVRGBA::VertexUVRGBA(float x, float y, float z, float u, float v, RGBA rgba)
		: Vertex(x, y, z), _u(u), _v(v), _r(rgba._r), _g(rgba._g), _b(rgba._b), _a(rgba._a)
	{}
	VertexUVRGBA::VertexUVRGBA(Vertex vertex, float u, float v, RGBA rgba)
		: Vertex(vertex), _u(u), _v(v), _r(rgba._r), _g(rgba._g), _b(rgba._b), _a(rgba._a)
	{}
	VertexUVRGBA::VertexUVRGBA(VertexRGBA vertex, float u, float v)
		: Vertex(vertex), _u(u), _v(v), _r(vertex._r), _g(vertex._g), _b(vertex._b), _a(vertex._a)
	{}
	VertexUVRGBA::VertexUVRGBA(float x, float y, float z, UV uv, RGBA rgba)
		: Vertex(x, y, z), _u(uv._u), _v(uv._v), _r(rgba._r), _g(rgba._g), _b(rgba._b), _a(rgba._a)
	{}
	VertexUVRGBA::VertexUVRGBA(Vertex vertex, UV uv, RGBA rgba)
		: Vertex(vertex), _u(uv._u), _v(uv._v), _r(rgba._r), _g(rgba._g), _b(rgba._b), _a(rgba._a)
	{}
	VertexUVRGBA::VertexUVRGBA(VertexUV vertex, RGBA rgba)
		: Vertex(vertex), _u(vertex._u), _v(vertex._v), _r(rgba._r), _g(rgba._g), _b(rgba._b), _a(rgba._a)
	{}
	VertexUVRGBA::VertexUVRGBA(VertexRGBA vertex, UV uv)
		: Vertex(vertex), _u(uv._u), _v(uv._v), _r(vertex._r), _g(vertex._g), _b(vertex._b), _a(vertex._a)
	{}
	VertexUVRGBA::VertexUVRGBA(const Vertex& vertex)
		: Vertex(vertex), _u(0.0f), _v(0.0f), _r(1.0f), _g(1.0f), _b(1.0f), _a(1.0f)
	{}
	VertexUVRGBA::VertexUVRGBA(const VertexUV& vertex)
		: Vertex(vertex._x, vertex._y, vertex._z), _u(vertex._u), _v(vertex._v), _r(1.0f), _g(1.0f), _b(1.0f), _a(1.0f)
	{}
	VertexUVRGBA::VertexUVRGBA(const VertexRGBA& vertex)
		: Vertex(vertex._x, vertex._y, vertex._z), _u(0.0f), _v(0.0f), _r(vertex._r), _g(vertex._g), _b(vertex._b), _a(vertex._a)
	{}
	bool VertexUVRGBA::operator==(const VertexUVRGBA& v) const
	{
		return _x == v._x && _y == v._y && _z == v._z && _r == v._r && _g == v._g && _b == v._b && _a == v._a;
	}
	bool VertexUVRGBA::operator<(const Graphics::VertexUVRGBA& v) const
	{
		if(_x == v._x)
		{
			if(_y == v._y)
			{
				if(_z == v._z)
				{
					if(_u == v._u)
					{
						if(_v == v._v)
						{
							if(_r == v._r)
							{
								if(_g == v._g)
								{
									if(_b == v._b)
									{
										return _a < v._a;
									}
									else
									{
										return _b < v._b;
									}
								}
								else
								{
									return _g < v._g;
								}
							}
							else
							{
								return _r < v._r;
							}
						}
						else
						{
							return _v < v._v;
						}
					}
					else
					{
						return _u < v._u;
					}
				}
				else
				{
					return _z < v._z;
				}
			}
			else
			{
				return _y < v._y;
			}
		}
		else
		{
			return _x < v._x;
		}
	}
}
