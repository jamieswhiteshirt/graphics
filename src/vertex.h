#ifndef VERTEX_H
#define VERTEX_H

namespace Graphics
{
	class SkeletalWeight
	{
	public:
		float _weight0, _weight1;
		unsigned char _index0, _index1;

		SkeletalWeight(float weight0 = 0.0f, float weight1 = 0.0f, unsigned char index0 = 0, unsigned char index1 = 0);
	};

	class UV
	{
	public:
		float _u, _v;
		UV(float u = 0.0f, float v = 0.0f);
		
		bool operator==(const UV& uv) const;
		bool operator<(const UV& uv) const;
	};

	class RGBA
	{
	public:
		float _r, _g, _b, _a;
		RGBA(float r = 1.0f, float g = 1.0f, float b = 1.0f, float a = 1.0f);

		bool operator==(const RGBA& rgba) const;
		bool operator<(const RGBA& rgba) const;
	};
	
	class Vertex
	{
	public:
		float _x, _y, _z;
		Vertex(float x = 0.0f, float y = 0.0f, float z = 0.0f);

		bool operator==(const Vertex& v) const;
		bool operator<(const Vertex& v) const;
	};

	class VertexUV : public Vertex
	{
	public:
		float _u, _v;
		VertexUV(float x = 0.0f, float y = 0.0f, float z = 0.0f, float u = 0.0f, float v = 0.0f);
		VertexUV(Vertex vertex, float u, float v);
		VertexUV(float x, float y, float z, UV uv);
		VertexUV(Vertex vertex, UV uv);
		VertexUV(const Vertex& vertex);

		bool operator==(const VertexUV& v) const;
		bool operator<(const VertexUV& v) const;
	};

	class VertexRGBA : public Vertex
	{
	public:
		float _r, _g, _b, _a;
		VertexRGBA(float x = 0.0f, float y = 0.0f, float z = 0.0f, float r = 1.0f, float g = 1.0f, float b = 1.0f, float a = 1.0f);
		VertexRGBA(Vertex vertex, float r, float g, float b, float a);
		VertexRGBA(float x, float y, float z, RGBA rgba);
		VertexRGBA(Vertex vertex, RGBA rgba);
		VertexRGBA(const Vertex& vertex);

		bool operator==(const VertexRGBA& v) const;
		bool operator<(const VertexRGBA& v) const;
	};

	class VertexUVRGBA : public Vertex
	{
	public:
		float _u, _v, _r, _g, _b, _a;
		VertexUVRGBA(float x = 0.0f, float y = 0.0f, float z = 0.0f, float u = 0.0f, float v = 0.0f, float r = 1.0f, float g = 1.0f, float b = 1.0f, float a = 1.0f);
		VertexUVRGBA(Vertex vertex, float u, float v, float r, float g, float b, float a);
		VertexUVRGBA(float x, float y, float z, UV uv, float r, float g, float b, float a);
		VertexUVRGBA(Vertex vertex, UV uv, float r, float g, float b, float a);
		VertexUVRGBA(VertexUV vertex, float r, float g, float b, float a);
		VertexUVRGBA(float x, float y, float z, float u, float v, RGBA rgba);
		VertexUVRGBA(Vertex vertex, float u, float v, RGBA rgba);
		VertexUVRGBA(VertexRGBA vertex, float u, float v);
		VertexUVRGBA(float x, float y, float z, UV uv, RGBA rgba);
		VertexUVRGBA(Vertex vertex, UV uv, RGBA rgba);
		VertexUVRGBA(VertexUV vertex, RGBA rgba);
		VertexUVRGBA(VertexRGBA vertex, UV uv);
		VertexUVRGBA(const Vertex& vertex);
		VertexUVRGBA(const VertexUV& vertex);
		VertexUVRGBA(const VertexRGBA& vertex);

		bool operator==(const VertexUVRGBA& v) const;
		bool operator<(const VertexUVRGBA& v) const;
	};
}

#endif
