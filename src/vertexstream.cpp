#include "vertexstream.h"
#include "graphicserror.h"
#include "shader.h"
#include <stdexcept>

namespace Graphics
{
	AttributeDataStreamBase::AttributeDataStreamBase()
	{}

	AttributeDataStreamBase::VertexAttribute::VertexAttribute(GLuint index, GLint size, GLenum type, GLsizei offset)
		: _index(index), _size(size), _type(type), _offset(offset)
	{}

	AttributeDataStreamBase::VertexAttributeF::VertexAttributeF(GLuint index, GLint size, GLenum type, GLsizei offset, GLboolean normalized)
		: VertexAttribute(index, size, type, offset), _normalized(normalized)
	{}

	AttributeDataStreamBase::VertexAttributeI::VertexAttributeI(GLuint index, GLint size, GLenum type, GLsizei offset)
		: VertexAttribute(index, size, type, offset)
	{}

	template<typename datatype>
	void AttributeDataStream<datatype>::_upload()
	{
		if(_uploaded) return;

		glGenBuffers(1, &_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, _vbo);
		glBufferData(GL_ARRAY_BUFFER, _data.size() * sizeof(datatype), _data.data(), GL_STATIC_DRAW);

		for(auto iter = _floatAttributes.begin(); iter != _floatAttributes.end(); ++iter)
		{
			glEnableVertexAttribArray(iter->_index);
			glVertexAttribPointer(iter->_index, iter->_size, iter->_type, iter->_normalized, sizeof(datatype), (const GLvoid*)iter->_offset);
		}
		for(auto iter = _integerAttributes.begin(); iter != _integerAttributes.end(); ++iter)
		{
			glEnableVertexAttribArray(iter->_index);
			glVertexAttribIPointer(iter->_index, iter->_size, iter->_type, sizeof(datatype), (const GLvoid*)iter->_offset);
		}

		_uploaded = true;
	}
	template<typename datatype>
	AttributeDataStream<datatype>::AttributeDataStream(bool addDefaultAttributes)
		: _vbo(0), _uploaded(false)
	{
		if(addDefaultAttributes)
		{
			_addDefaultAttributes();
		}
	}
	template<typename datatype>
	AttributeDataStream<datatype>::~AttributeDataStream()
	{
		if(_uploaded)
		{
			glDeleteBuffers(1, &_vbo);
		}
	}
	template<typename datatype>
	size_t AttributeDataStream<datatype>::count()
	{
		return _data.size();
	}
	template<typename datatype>
	void AttributeDataStream<datatype>::addFloatVertexAttribute(GLuint index, GLint size, GLenum type, GLsizei offset, GLboolean normalized)
	{
		if(_uploaded) throw std::runtime_error("A float attribute was attempted added to an already uploaded VertexInputStream!");
		_floatAttributes.push_back(VertexAttributeF(index, size, type, offset, normalized));
	}
	template<typename datatype>
	void AttributeDataStream<datatype>::addIntegerVertexAttribute(GLuint index, GLint size, GLenum type, GLsizei offset)
	{
		if(_uploaded) throw std::runtime_error("An integer attribute was attempted added to an already uploaded VertexInputStream!");
		_integerAttributes.push_back(VertexAttributeI(index, size, type, offset));
	}
	template<typename datatype>
	void AttributeDataStream<datatype>::addRange(datatype* data, size_t length)
	{
		if(_uploaded) throw std::runtime_error("A set of data was attempted added to an already uploaded VertexInputStream!");
		_data.reserve(_data.size() + length);
		for(size_t i = 0; i < length; i++)
		{
			_data.push_back(data[i]);
		}
	}

	template<>
	void AttributeDataStream<SkeletalWeight>::_addDefaultAttributes()
	{
		addFloatVertexAttribute(4, 2, GL_FLOAT, 0, GL_FALSE);
		addIntegerVertexAttribute(5, 2, GL_UNSIGNED_BYTE, sizeof(float) * 2);
	}
	template<>
	void AttributeDataStream<UV>::_addDefaultAttributes()
	{
		addFloatVertexAttribute(1, 2, GL_FLOAT, 0, GL_FALSE);
	}
	template<>
	void AttributeDataStream<utility::Normal>::_addDefaultAttributes()
	{
		addFloatVertexAttribute(3, 3, GL_FLOAT, 0, GL_TRUE);
	}
	template<>
	void AttributeDataStream<Vertex>::_addDefaultAttributes()
	{
		addFloatVertexAttribute(0, 3, GL_FLOAT, 0, GL_FALSE);
	}
	template<>
	void AttributeDataStream<VertexUV>::_addDefaultAttributes()
	{
		addFloatVertexAttribute(0, 3, GL_FLOAT, 0, GL_FALSE);
		addFloatVertexAttribute(1, 2, GL_FLOAT, sizeof(float) * 3, GL_FALSE);
	}
	template<>
	void AttributeDataStream<VertexRGBA>::_addDefaultAttributes()
	{
		addFloatVertexAttribute(0, 3, GL_FLOAT, 0, GL_FALSE);
		addFloatVertexAttribute(2, 4, GL_FLOAT, sizeof(float) * 3, GL_FALSE);
	}
	template<>
	void AttributeDataStream<VertexUVRGBA>::_addDefaultAttributes()
	{
		addFloatVertexAttribute(0, 3, GL_FLOAT, 0, GL_FALSE);
		addFloatVertexAttribute(1, 2, GL_FLOAT, sizeof(float) * 3, GL_FALSE);
		addFloatVertexAttribute(2, 4, GL_FLOAT, sizeof(float) * 5, GL_FALSE);
	}

	GLuint VertexStream::_shortestAttributeDataCount()
	{
		GLuint shortest = 0;
		for(unsigned int i = 0; i < _attributeDataStreams.size(); i++)
		{
			GLuint count = _attributeDataStreams[i]->count();
			if(i == 0 || count < shortest)
			{
				shortest = count;
			}
		}

		return shortest;
	}
	void VertexStream::_upload()
	{
		if(_uploaded) return;

		glGenVertexArrays(1, &_vao);
		glBindVertexArray(_vao);

		if(_indexed)
		{
			glGenBuffers(1, &_ibo);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _ibo);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(GLushort), _indices.data(), GL_STATIC_DRAW);
		}

		ifErrorThrow("RenderStream::upload");

		for(auto iter = _attributeDataStreams.begin(); iter != _attributeDataStreams.end(); ++iter)
		{
			iter->get()->_upload();
		}

		ifErrorThrow("RenderStream::upload");

		_uploaded = true;
	}
	void VertexStream::_prepareForRender()
	{
		_upload();
		if(ShaderProgram::getCurrent() == nullptr) throw std::runtime_error("GL ERROR: No shader program bound");
		ShaderProgram::getCurrent()->uploadUniforms();
	}
	VertexStream::VertexStream(bool indexed)
		: _vao(0), _ibo(0), _indexed(indexed), _uploaded(false)
	{}
	VertexStream::~VertexStream()
	{
		if(_uploaded)
		{
			glDeleteVertexArrays(1, &_vao);
			if(_indexed) glDeleteBuffers(1, &_ibo);
		}
	}
	void VertexStream::addAttributeDataStream(std::shared_ptr<AttributeDataStreamBase> attributeDataStream)
	{
		if(_uploaded) throw std::runtime_error("VertexStream is already uploaded");
		_attributeDataStreams.push_back(attributeDataStream);
	}
	void VertexStream::addRange(unsigned short* indices, size_t length)
	{
		if(_uploaded) throw std::runtime_error("VertexStream is already uploaded");
		if(!_indexed) throw std::runtime_error("VertexStream is not indexed");
		_indices.reserve(_indices.size() + length);
		for(size_t i = 0; i < length; i++)
		{
			_indices.push_back(indices[i]);
		}
	}
	void VertexStream::render(GLuint count, GLuint first, GLenum mode)
	{
		_prepareForRender();

		glBindVertexArray(_vao);

		if(_indexed)
		{
			if(count == 0) count = _indices.size() - first;
			if(count > 0)
			{
				glDrawElements(mode, count, GL_UNSIGNED_SHORT, (const void*)(first * sizeof(GLushort)));
			}
		}
		else
		{
			if(count == 0) count = _shortestAttributeDataCount() - first;
			if(count > 0)
			{
				glDrawArrays(mode, first, count);
			}
		}
	}

	VertexStream& operator<<(VertexStream& vertexStream, const GLushort index)
	{
		if(vertexStream._uploaded) throw std::runtime_error("VertexStream is already uploaded");
		if(!vertexStream._indexed) throw std::runtime_error("VertexStream is not indexed");
		vertexStream._indices.push_back(index);
		return vertexStream;
	}
	
	template class AttributeDataStream<SkeletalWeight>;
	template class AttributeDataStream<UV>;
	template class AttributeDataStream<utility::Normal>;
	template class AttributeDataStream<Vertex>;
	template class AttributeDataStream<VertexUV>;
	template class AttributeDataStream<VertexRGBA>;
	template class AttributeDataStream<VertexUVRGBA>;
}