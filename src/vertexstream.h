#ifndef VERTEXSTREAM_H
#define VERTEXSTREAM_H

#include "vertex.h"
#include <GL/glew.h>
#include <vector>
#include <memory>

namespace Graphics
{
	class AttributeDataStreamBase
	{
	private:
		virtual void _upload() = 0;

	protected:
		struct VertexAttribute
		{
			const GLuint _index;
			const GLint _size;
			const GLenum _type;
			const GLsizei _offset;

			VertexAttribute(GLuint index, GLint size, GLenum type, GLsizei offset = 0);
		};

		struct VertexAttributeF : public VertexAttribute
		{
			const GLboolean _normalized;
			VertexAttributeF(GLuint index, GLint size, GLenum type, GLsizei offset = 0, GLboolean normalized = GL_FALSE);
		};

		struct VertexAttributeI : public VertexAttribute
		{
			VertexAttributeI(GLuint index, GLint size, GLenum type, GLsizei offset = 0);
		};

		AttributeDataStreamBase();

	public:
		virtual size_t count() = 0;

		friend class VertexStream;
	};

	template<typename datatype>
	class AttributeDataStream : public AttributeDataStreamBase
	{
	private:
		GLuint _vbo;
		bool _uploaded;

		std::vector<datatype> _data;
		std::vector<VertexAttributeF> _floatAttributes;
		std::vector<VertexAttributeI> _integerAttributes;

		virtual void _upload();
		void _addDefaultAttributes();

	public:
		AttributeDataStream(bool addDefaultAttributes = true);
		~AttributeDataStream();

		virtual size_t count();

		void addFloatVertexAttribute(GLuint index, GLint size, GLenum type, GLsizei offset = 0, GLboolean normalized = GL_FALSE);
		void addIntegerVertexAttribute(GLuint index, GLint size, GLenum type, GLsizei offset = 0);
		void addRange(datatype* data, size_t length);

		friend unsigned short operator<<(AttributeDataStream<datatype>& attributeDataStream, const datatype& data)
		{
			if(attributeDataStream._uploaded) throw std::runtime_error("A piece of data was attempted added to an already uploaded VertexInputStream!");
			attributeDataStream._data.push_back(data);
			return attributeDataStream._data.size() - 1;
		}

		friend class VertexStream;
	};

	class VertexStream
	{
	private:
		GLuint _vao;
		GLuint _ibo;

		std::vector<std::shared_ptr<AttributeDataStreamBase>> _attributeDataStreams;
		std::vector<GLushort> _indices;
		const bool _indexed;
		bool _uploaded;

		GLuint _shortestAttributeDataCount();
		void _upload();
		void _prepareForRender();
		
		friend VertexStream& operator<<(VertexStream&, const GLushort);
	protected:
		void _applyAttributes();
	public:
		VertexStream(bool indexed = false);
		virtual ~VertexStream();

		void addAttributeDataStream(std::shared_ptr<AttributeDataStreamBase> attributeDataStream);
		void addRange(unsigned short* indices, size_t length);
		void render(GLuint count = 0, GLuint first = 0, GLenum mode = GL_TRIANGLES);
	};
	
	VertexStream& operator<<(VertexStream& vertexStream, const GLushort);
}

#endif