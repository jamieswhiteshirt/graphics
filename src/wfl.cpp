#include "wfl.h"
#include <string.h>
#include <sstream>
#include <map>
#include <algorithm>

struct Face
{
	unsigned short v[3];
	unsigned short uv[3];
	Face(unsigned short v0 = 0, unsigned short v1 = 0, unsigned short v2 = 0, unsigned short uv0 = 0, unsigned short uv1 = 0, unsigned short uv2 = 0)
	{
		v[0] = v0;
		v[1] = v1;
		v[2] = v2;
		uv[0] = uv0;
		uv[1] = uv1;
		uv[2] = uv2;
	}
};

namespace Graphics
{
	namespace WFL
	{
		const char* OBJToWFL(const char* dataOBJ, size_t sizeOBJ, size_t& sizeWFL)
		{
			std::vector<Vertex> rawVertices;
			std::vector<UV> rawUVs;
			std::vector<Face> rawFaces;

			std::istringstream s0(std::string(dataOBJ, sizeOBJ));
			std::string line;
			while(std::getline(s0, line))
			{
				if(line.size() == 0) continue;

				std::istringstream s1(line);

				std::string type;
				s1 >> type;

				std::string num;

				if(type == "v")
				{
					//This is a vertex definition
					s1 >> num;
					float x = std::stof(num);
					s1 >> num;
					float y = std::stof(num);
					s1 >> num;
					float z = std::stof(num);

					rawVertices.push_back(Vertex(x, y, z));
				}
				else if(type == "vt")
				{
					//This is a texture coordinate definition
					s1 >> num;
					float u = std::stof(num);
					s1 >> num;
					float v = std::stof(num);

					rawUVs.push_back(UV(u, v));
				}
				else if(type == "f")
				{
					//This is a face definition. It is assumed to be a triangle

					unsigned short vs[3];
					unsigned short uvs[3];

					for(unsigned int i = 0; i < 3; i++)
					{
						s1 >> num;

						size_t slashIndex = num.find_last_of('/');
						if(slashIndex != std::string::npos)
						{
							//This face definition has pointers to UV coordinates
							vs[i] = std::stoi(num.substr(0, num.find_first_of('/'))) - 1;
							uvs[i] = std::stoi(num.substr(slashIndex + 1, std::string::npos)) - 1;
						}
						else
						{
							//This face definition does not have pointers to UV coordinates
							vs[i] = std::stoi(num) - 1;
							uvs[i] = 0;
						}
					}

					rawFaces.push_back(Face(vs[0], vs[1], vs[2], uvs[0], uvs[1], uvs[2]));
				}
			}

			char* dataWFL = nullptr;
			unsigned int flags = 0;
			unsigned short numVertices = 0;
			unsigned short numIndices = 0;
			size_t vertexSize = 0;

			std::vector<unsigned short> indices;

			if(rawUVs.size() > 0)
			{
				//This model has UV definitions. Because OBJ UVs are not per-vertex and instead in an independent list, a new list of vertices has to be generated for converting to vertices that contain UVs.
				flags |= WFL_UV;

				vertexSize = sizeof(VertexUV);

				//A list of vertices with UVs. Each vertex is always unique
				std::vector<VertexUV> tempVertices;

				for(unsigned int i = 0; i < rawFaces.size(); i++)
				{
					for(unsigned int j = 0; j < 3; j++)
					{
						VertexUV v(rawVertices[rawFaces[i].v[j]]);
						v._u = rawUVs[rawFaces[i].uv[j]]._u;
						v._v = rawUVs[rawFaces[i].uv[j]]._v;


						auto itr = std::find(tempVertices.begin(),tempVertices.end(),v);

						unsigned short index = 0;
						if(itr == tempVertices.end())
						{
							tempVertices.push_back(v);
							index = tempVertices.size() - 1;
						}
						else
						{
							index = itr - tempVertices.begin();
						}

						indices.push_back(index);
					}
				}

				std::vector<VertexUV> vertices;

				const size_t nonIndexedSize = 16 + sizeof(VertexUV) * 3 * rawFaces.size();
				const size_t indexedSize = 16 + sizeof(VertexUV) * tempVertices.size() + sizeof(unsigned short) * 3 * rawFaces.size();

				if(nonIndexedSize <= indexedSize)
				{
					//This model is more efficient as a non-indexed model
					sizeWFL = nonIndexedSize;
					dataWFL = new char[sizeWFL];

					vertices.reserve(indices.size());
					for(unsigned int i = 0; i < indices.size(); i++)
					{
						vertices.push_back(tempVertices[indices[i]]);
					}

					indices.clear();

					numVertices = vertices.size();

					memcpy(&dataWFL[16], vertices.data(), sizeof(VertexUV) * numVertices);
				}
				else
				{
					//This model will have its indexation preserved
					sizeWFL = indexedSize;
					dataWFL = new char[sizeWFL];
					
					numVertices = tempVertices.size();

					memcpy(&dataWFL[16], tempVertices.data(), sizeof(VertexUV) * numVertices);
				}

				numIndices = indices.size();
			}
			else
			{
				vertexSize = sizeof(Vertex);

				std::vector<Vertex> vertices;

				const size_t nonIndexedSize = 16 + sizeof(Vertex) * 3 * rawFaces.size();
				const size_t indexedSize = 16 + sizeof(Vertex) * rawVertices.size() + sizeof(unsigned short) * 3 * rawFaces.size();

				if(nonIndexedSize <= indexedSize)
				{
					//This model is more efficient as a non-indexed model
					sizeWFL = nonIndexedSize;
					dataWFL = new char[sizeWFL];

					vertices.reserve(rawFaces.size());
					for(unsigned int i = 0; i < rawFaces.size(); i++)
					{
						for(unsigned int j = 0; j < 3; j++)
						{
							vertices.push_back(rawVertices[rawFaces[i].v[j]]);
						}
					}
				}
				else
				{
					//This model will have its indexation preserved
					sizeWFL = indexedSize;
					dataWFL = new char[sizeWFL];

					vertices = rawVertices;
					for(unsigned int i = 0; i < rawFaces.size(); i++)
					{
						for(unsigned int j = 0; j < 3; j++)
						{
							indices.push_back(rawFaces[i].v[j]);
						}
					}
				}

				numVertices = vertices.size();
				numIndices = indices.size();

				memcpy(&dataWFL[16], &vertices[0], sizeof(Vertex) * numVertices);
			}

			if(!indices.empty())
			{
				memcpy(&dataWFL[16 + vertexSize * numVertices], &indices[0], sizeof(unsigned short) * numIndices);
			}

			const char* waffles = "WAFFLES!";

			memcpy(dataWFL, waffles, 8);
			memcpy(&dataWFL[8], &flags, sizeof(unsigned int));
			memcpy(&dataWFL[12], &numVertices, sizeof(unsigned short));
			memcpy(&dataWFL[14], &numIndices, sizeof(unsigned short));

			return dataWFL;
		}
		VertexStream* vertexStreamFromWFL(unsigned char* dataWFL, size_t sizeWFL)
		{
			if(memcmp(dataWFL, "WAFFLES!", 8) != 0) throw std::runtime_error("Invalid WFL file header");

			unsigned int flags = 0;
			unsigned short numVertices = 0;
			unsigned short numIndices = 0;
			memcpy(&flags, &dataWFL[8], sizeof(unsigned int));
			memcpy(&numVertices, &dataWFL[12], sizeof(unsigned short));
			memcpy(&numIndices, &dataWFL[14], sizeof(unsigned short));

			bool isUV = (flags & WFL_UV) != 0;
			bool isRGBA = (flags & WFL_RGBA) != 0;

			const size_t vertexSize = isUV ? (isRGBA ? sizeof(VertexUVRGBA) : sizeof(VertexUV)) : (isRGBA ? sizeof(VertexRGBA) : sizeof(Vertex));
			const size_t expectedSize = 16 + vertexSize * numVertices + sizeof(unsigned short) * numIndices;

			if(sizeWFL != expectedSize) throw std::runtime_error("Invalid WFL file size");

			bool isIndexed = numIndices > 0;
			VertexStream* vertexStream = new VertexStream(isIndexed);

			if(isUV)
			{
				if(isRGBA)
				{
					std::shared_ptr<AttributeDataStream<VertexUVRGBA>> attributeDataStream = std::shared_ptr<AttributeDataStream<VertexUVRGBA>>(new AttributeDataStream<VertexUVRGBA>());
					attributeDataStream->addRange((VertexUVRGBA*)&dataWFL[16], numVertices);
					vertexStream->addAttributeDataStream(attributeDataStream);
				}
				else
				{
					std::shared_ptr<AttributeDataStream<VertexUV>> attributeDataStream = std::shared_ptr<AttributeDataStream<VertexUV>>(new AttributeDataStream<VertexUV>());
					attributeDataStream->addRange((VertexUV*)&dataWFL[16], numVertices);
					vertexStream->addAttributeDataStream(attributeDataStream);
				}
			}
			else
			{
				if(isRGBA)
				{
					std::shared_ptr<AttributeDataStream<VertexRGBA>> attributeDataStream = std::shared_ptr<AttributeDataStream<VertexRGBA>>(new AttributeDataStream<VertexRGBA>());
					attributeDataStream->addRange((VertexRGBA*)&dataWFL[16], numVertices);
					vertexStream->addAttributeDataStream(attributeDataStream);
				}
				else
				{
					std::shared_ptr<AttributeDataStream<Vertex>> attributeDataStream = std::shared_ptr<AttributeDataStream<Vertex>>(new AttributeDataStream<Vertex>());
					attributeDataStream->addRange((Vertex*)&dataWFL[16], numVertices);
					vertexStream->addAttributeDataStream(attributeDataStream);
				}
			}

			if(isIndexed)
			{
				vertexStream->addRange((unsigned short*)&dataWFL[16 + vertexSize * numVertices], numIndices);
			}

			return vertexStream;
		}
	};
};
