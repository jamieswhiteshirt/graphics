#ifndef WFL_H
#define WFL_H

#define WFL_UV 0x1
#define WFL_RGBA 0x2

#include "vertexstream.h"

namespace Graphics
{
	namespace WFL
	{
		const char* OBJToWFL(const char* data, size_t size, size_t& sizeWFL);
		VertexStream* vertexStreamFromWFL(unsigned char* data, size_t size);
	}
}

#endif