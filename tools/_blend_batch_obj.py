import bpy, sys, argparse, os, subprocess

argv = sys.argv
argv = argv[argv.index("--") + 1:] if '--' in argv else []

parser = argparse.ArgumentParser(description='Batch export all objects in blend file to obj files')
parser.add_argument('--out', '-o', nargs=1, default='.', type=str)
parser.add_argument('--wfl', '-w', action='store_true')

args = parser.parse_args(argv)

bpy.ops.object.select_all(action='DESELECT')    
scene = bpy.context.scene
for ob in scene.objects:
	scene.objects.active = ob
	ob.select = True
	if ob.type == 'MESH':
		filename = os.path.join(args.out[0], ob.name)
		tmp_name = './.tmp.obj'
		bpy.ops.export_scene.obj(
				filepath=filename + '.obj' if not args.wfl else tmp_name,
				check_existing=False,
				use_selection=True,
				use_normals=False,
				use_materials=True,
				use_edges=True,
				use_uvs=True,
				use_triangles=True,
				axis_forward='-Z',
				axis_up='Y',
				keep_vertex_order=True
			)
		if args.wfl:
			subprocess.call(['objtowfl', '-i', tmp_name, '-o', filename + '.wfl'])
			print('Converted obj to wfl: %s' % (filename + '.wfl'))
			os.remove(tmp_name)
	ob.select = False
