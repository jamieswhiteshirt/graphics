#!/usr/bin/python3

import os, argparse, subprocess

parser = argparse.ArgumentParser(description='Batch export all objects in blend file to obj files')
parser.add_argument('--out', '-o', nargs=1, default='.', type=str)
parser.add_argument('--wfl', '-w', action='store_true')
parser.add_argument('blendfile', type=str)

args = parser.parse_args()
exec_dir = os.path.dirname(os.path.realpath(__file__))

call = ['blender', '-b', args.blendfile, '-P', os.path.join(exec_dir, '_blend_batch_obj.py'), '--', '-o', args.out[0]]
if args.wfl:
	call.append('--wfl')
subprocess.call(call)

