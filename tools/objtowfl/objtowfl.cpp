#include <string>
#include <iostream>
#include <istream>
#include <fstream>
#include <vector>
#include <iterator>
#include <wfl.h>

static std::string inputFile = "";
static std::string outputFile = "";

struct Argument;

static Argument* getArgumentFromName(std::string name);
static void parseArgs(int argc, const char* argv[]);
static void printHelp();

struct Argument {
	std::string shortName;
	std::string longName;
	std::string description;
	int numArgs;
	void(*function)(std::vector<std::string> args);
} arguments[] = {
	{ "i", "input", "The input file", 1,
		[](std::vector<std::string> args) { inputFile = args[0]; } },
	{ "o", "output", "The output file", 1,
		[](std::vector<std::string> args) { outputFile = args[0]; } },
	{ "h", "help", "Print help text", 0,
		[](std::vector<std::string> args) { printHelp(); exit(0); } },
};
const size_t arguments_size = sizeof(arguments) / sizeof(arguments[0]);

static Argument* getArgumentFromName(std::string name) {
	for (int i = 0; i < arguments_size; i++) {
		if ("-" + arguments[i].shortName == name ||
			"--"+ arguments[i].longName == name)
			return &arguments[i];
	}
	return nullptr;
}

void parseArgs(int argc, const char* argv[]) {
	for (int i = 0; i < argc; i++) {
		Argument* arg = getArgumentFromName(argv[i]);
		if (arg) {
			std::vector<std::string> args;
			for (int j = 0; j < arg->numArgs; j++) {
				i++;
				if (i >= argc) {
					std::cerr << "Missing arguments for '" << arg->longName << "'" << std::endl;
					exit(1);
				}
				args.push_back(argv[i]);
			}
			arg->function(args);
		}
	}
}

void printHelp() {
	std::cerr << "This will become the help text!" << std::endl;
}

std::string readEntireStream(std::istream& s) {
	s >> std::noskipws;
	std::istream_iterator<char> it(s);
	std::istream_iterator<char> end;
	return std::string(it, end);
}

std::string readFile(std::string path) {
	if (path != "") {
		std::fstream fs (path, std::fstream::in | std::fstream::binary);
		if (!fs.good()) {
			std::cerr << "Could not open file '" << path << "' for reading" << std::endl;
			exit(1);
		}
		return readEntireStream(fs);
	}
	else {
		return readEntireStream(std::cin);
	}
}

void writeToStream(std::ostream& s, const char* data, size_t size) {
	s.write(data, size);
}

void writeFile(std::string path, const char* data, size_t size) {
	if (path != "") {
		std::fstream fs (path, std::fstream::out | std::fstream::trunc | std::fstream::binary);
		if (!fs.good()) {
			std::cerr << "Could not open file '" << path << "' for reading" << std::endl;
			exit(1);
		}
		writeToStream(fs, data, size);
	}
	else {
		writeToStream(std::cout, data, size);
	}
}

int main(int argc, const char* argv[]) {
	parseArgs(argc, argv);
	std::string inputData = readFile(inputFile);
	size_t resultSize;
	const char* result = Graphics::WFL::OBJToWFL(inputData.c_str(), inputData.size(), resultSize);
	writeFile(outputFile, result, resultSize);
	delete[] result;
	return 0;
}

